[DBus(name="sh.carbon.update1.Progress")]
public interface Updated.Progress : DBusProxy {
    // This is hooked directly into OSTree's journal-msg signal
    public signal void journal_message(string message);

    // Signals when the daemon is creating the sysroot's directory structure
    public signal void creating_dirs();

    // Signals when the daemon is finding remotes
    // rollback uses this to signal that it's looking for a victim deployment
    public signal void resolving();

    // Signals when the daemon is starting a pull
    public signal void pulling();

    // Signals how much the daemon has downloaded in the ongoing pull.
    // Reports exact byte amounts. If exact sizes are unavailable, coarse
    // updates will still be emitted
    public signal void downloaded(uint64 done_bytes, uint64 total_bytes);

    // Signals how many objects the daemon has downloaded in the ongoing pull.
    // Make sure to ignore coarse download updates if fine data is available.
    public signal void downloaded_coarse(uint done_objs, uint total_objs);

    // Signals when the daemon is merging commits together
    public signal void merging();

    // Signals when the daemon is deploying an image
    // rollback uses this to signal that it's removing the victim deployment
    public signal void deploying();

    // Signals when the daemon is cleaning up the repo
    public signal void pruning();

    // Signals whenever the current task has finished
    public signal void finished();
}

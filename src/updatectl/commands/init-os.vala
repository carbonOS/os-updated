using Cli;

class InitOSCommand : SubCommand {
	// Configure the subcommand
	construct {
		name = "init-os";
		desc = "Initialize a new OS installation in TARGET from SOURCE";
		arguments = "--sysroot=TARGET SOURCE";
		expected_args = 1;
		error_prefix = "Failed to initialize OS";
	}

	// Entry point into the subcommand
	public override int main(string[] args) throws Error {
	    var daemon = Daemon.get_main_service();
        var progress = new Daemon.TaskOutput(daemon);
        progress.pulling_msg = "Copying content";

        var source = Filename.canonicalize(args[1]);
        if (opt_sysroot == "")
            return print_opt_error("--sysroot must be specified explicitly", args);
        if (!FileUtils.test(opt_sysroot, FileTest.IS_DIR))
            return print_error("TARGET must be an empty directory");

        print("A copy of %s will be installed from %s into %s\n",
            daemon.os_name, source, opt_sysroot);
	    if (!prompt_yN("Are you sure you want to make these changes?")) {
	        print("Aborting.\n");
	        return 0;
	    }

        bool done = false;
        daemon.init_os.begin(opt_sysroot, source, (obj, res) => {
            try {
                daemon.init_os.end(res);
                done = true;
            } catch (Error e) {
                progress.failed(e);
            }
        });
        while (!done) progress.iteration();

		return 0;
	}
}

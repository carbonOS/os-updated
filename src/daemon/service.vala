using Updated;

[DBus(name="sh.carbon.update1")]
class MainService : DBusProxy {

    public string version { owned get { return VERSION; } }
    public string os_name { owned get { return OS_NAME; } }

    public void release(BusName sender) throws Error {
        Daemon.unregister_client(null, sender);
    }

    public void cancel(BusName sender) throws Error {
        message("Canceling operation for client: %s", sender);
        Daemon.cancel_client(sender);
    }

    public ObjectPath get_progress(BusName sender) throws Error {
        return ((ProgressImpl) Daemon.get_progress(sender)).object_path;
    }

    public Deployment[] get_deployments(string opt_sysroot, out bool booted,
            BusName sender) throws Error {
        var cancellable = Daemon.get_cancellable(sender);
        var sysroot = OTUtils.get_sysroot_no_lock(opt_sysroot, cancellable);
        message("Returning status to %s", sender);
        return get_deployments_impl(sysroot, out booted);
    }

    public async CheckResult check(string opt_sysroot, BusName sender)
            throws Error {
        Daemon.error_if_live();
        var cancellable = Daemon.get_cancellable(sender);
        var progress = Daemon.get_progress(sender);
        message("Starting Check for %s", sender);

        yield Utils.auth(sender, "sh.carbon.update1.check", cancellable);
        message("Authorized Check for %s", sender);

        var sysroot = OTUtils.get_sysroot(opt_sysroot, cancellable);
        sysroot.journal_msg.connect(msg => progress.journal_message(msg));

        var result = yield check_impl(sysroot, progress, cancellable);
        message("Finished Check for %s", sender);
        return result;
    }

    public async void pull(string opt_sysroot, BusName sender) throws Error {
        Daemon.error_if_live();
        var cancellable = Daemon.get_cancellable(sender);
        var progress = Daemon.get_progress(sender);
        message("Starting Pull for %s", sender);

        yield Utils.auth(sender, "sh.carbon.update1.pull", cancellable);
        message("Authorized Pull for %s", sender);

        var sysroot = OTUtils.get_sysroot(opt_sysroot, cancellable);
        sysroot.journal_msg.connect(msg => progress.journal_message(msg));

        yield pull_impl(sysroot, null, progress, cancellable);
        message("Finished Pull for %s", sender);
    }

    public async void deploy(string opt_sysroot, BusName sender) throws Error {
        Daemon.error_if_live();
        var cancellable = Daemon.get_cancellable(sender);
        var progress = Daemon.get_progress(sender);
        message("Starting Deploy for %s", sender);

        yield Utils.auth(sender, "sh.carbon.update1.deploy", cancellable);
        message("Authorized Deploy for %s", sender);

        var sysroot = OTUtils.get_sysroot(opt_sysroot, cancellable);
        sysroot.journal_msg.connect(msg => progress.journal_message(msg));

        yield deploy_impl(sysroot, false, null, progress, cancellable);
        message("Finished Deploy for %s", sender);
    }

    public async bool rollback(string opt_sysroot, bool pin, BusName sender)
            throws Error {
        Daemon.error_if_live();
        var cancellable = Daemon.get_cancellable(sender);
        var progress = Daemon.get_progress(sender);
        message("Starting Rollback for %s", sender);

        yield Utils.auth(sender, "sh.carbon.update1.rollback", cancellable);
        message("Authorized Rollback for %s", sender);

        var sysroot = OTUtils.get_sysroot(opt_sysroot, cancellable);
        sysroot.journal_msg.connect(msg => progress.journal_message(msg));

        var reboot = yield rollback_impl(sysroot, pin, progress, cancellable);
        message("Finished Rollback for %s", sender);
        return reboot;
    }

    public async void factory_reset(string opt_sysroot, BusName sender)
            throws Error {
        Daemon.error_if_live();
        var cancellable = Daemon.get_cancellable(sender);
        var progress = Daemon.get_progress(sender);
        message("Starting FactoryReset for %s", sender);

        yield Utils.auth(sender, "sh.carbon.update1.factory-reset", cancellable);
        message("Authorized FactoryReset for %s", sender);

        var sysroot = OTUtils.get_sysroot(opt_sysroot, cancellable);
        sysroot.journal_msg.connect(msg => progress.journal_message(msg));

        // TODO: Ask systemd to isolate us
        // Ensure nothing gets in our way

        // TODO: Ask plymouth to tell the user we're factory resetting

        // TODO: Put a stamp file in stateroot
        // This allows us to resume the factory reset if we get inturrupted

        // Re-deploy the same OS image, but with no_merge=true
        // This wipes out all deployments and resets the contents of /etc
        yield deploy_impl(sysroot, true, null, progress, cancellable);

        // TODO: Delete everything from /var

        // TODO: Reboot
    }

    public async void switch_origin(string opt_sysroot, string base_variant,
            string kernel_variant, string collection, BusName sender)
            throws Error {
        Daemon.error_if_live();
        var cancellable = Daemon.get_cancellable(sender);
        var progress = Daemon.get_progress(sender);
        message("Starting Switch for %s (base=%s, kernel=%s, collection=%s)",
            sender, base_variant, kernel_variant, collection);

        // Validate the inputs
        var valid = new Regex("^(<keep>|[a-z]+)$");
        foreach (var arg in new string[]{base_variant, kernel_variant}) {
            if (!valid.match(arg))
                throw new DBusError.INVALID_ARGS(@"Malformed variant \"$arg\"");
        }
        valid = new Regex("^(<keep>|\\w+\\.\\w+\\.\\w+)$");
        if (!valid.match(collection))
            throw new DBusError.INVALID_ARGS("Malformed collection");

        yield Utils.auth(sender, "sh.carbon.update1.switch", cancellable);
        message("Authorized Switch for %s", sender);

        var sysroot = OTUtils.get_sysroot(opt_sysroot, cancellable);
        sysroot.journal_msg.connect(msg => progress.journal_message(msg));

        // Setup the new origin
        var merge = sysroot.get_merge_deployment(OS_NAME);
        var new_origin = OTUtils.get_origin(merge);
        if (base_variant != "<keep>")
            new_origin.set_string("origin", "BaseVariant", base_variant);
        if (kernel_variant != "<keep>")
            new_origin.set_string("origin", "KernelVariant", kernel_variant);
        if (collection != "<keep>")
            new_origin.set_string("origin", "Collection", collection);

        // Pull the new OS image
        message("Pulling for Switch for %s", sender);
        yield pull_impl(sysroot, new_origin, progress, cancellable);

        // Deploy the new OS image
        message("Deploying for Switch for %s", sender);
        yield deploy_impl(sysroot, false, new_origin, progress, cancellable);

        message("Finished Switch for %s", sender);
    }

    public async void set_pinned(string opt_sysroot, int deployment, bool pinned,
            BusName sender) throws Error {
        Daemon.error_if_live();
        var cancellable = Daemon.get_cancellable(sender);
        var progress = Daemon.get_progress(sender);
        message("Starting SetPinned for %s", sender);

        yield Utils.auth(sender, "sh.carbon.update1.set-pinned", cancellable);
        message("Authorized SetPinned for %s", sender);

        var sysroot = OTUtils.get_sysroot(opt_sysroot, cancellable);
        sysroot.journal_msg.connect(msg => progress.journal_message(msg));

        yield set_pinned_impl(sysroot, deployment, pinned, cancellable);
        message("Finished SetPinned for %s", sender);
    }

    public async void init_os(string opt_target, string opt_source, BusName sender)
            throws Error {
        var cancellable = Daemon.get_cancellable(sender);
        var progress = Daemon.get_progress(sender);
        message("Starting InitOs for %s: target=%s, source=%s", sender, opt_target,
            opt_source);

        yield Utils.auth(sender, "sh.carbon.update1.init-os", cancellable);
        message("Authorized InitOs for %s", sender);

        // Get the target sysroot
        if (opt_target == "")
            throw new DBusError.INVALID_ARGS("Target path is required");
        var target_file = File.new_for_path(opt_target);
        var target = new OSTree.Sysroot(target_file);
        target.set_mount_namespace_in_use();

        // Resolve the source & origin
        OSTree.Repo source_repo;
        OSTree.Deployment? source_merge_deploy = null;
        try {
            // If source is a sysroot, just use it directly
            var source = OTUtils.get_sysroot_no_lock(opt_source, cancellable);
            source_repo = source.repo();
            source_merge_deploy = source.get_merge_deployment(OS_NAME);
        } catch (Error e) {
            source_repo = new OSTree.Repo(File.new_for_path(opt_source));
            try {
                source_repo.open(cancellable);
            } catch {
                Utils.bail_if_cancelled(cancellable);
                throw new UpdateError.FAILED("Failed to open source repo");
            }
        }
        var origin = OTUtils.get_origin(source_merge_deploy);

        // Initialize & populate the OS
        init_os_impl(target, source_repo, origin, progress, cancellable);

        // Deploy the new OS image
        message("Deploying for InitOs for %s", sender);
        yield deploy_impl(target, true, origin, progress, cancellable);

        message("Finished InitOs for %s", sender);
    }
}

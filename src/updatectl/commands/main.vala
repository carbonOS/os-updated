using Cli;

public static string? opt_sysroot = null;

class MainCommand : SubCommand { // SubCommand is a bit of a misnomer here...
	private static bool version = false;
	private const OptionEntry[] _options = {
		{ "version", 'v', 0, OptionArg.NONE, ref version, "Display version number", null },
		{ "yes", 'y', 0, OptionArg.NONE, ref assume_yes, "Assume yes for all prompts", null },
		{ "sysroot", 0, 0, OptionArg.FILENAME, ref opt_sysroot, "Sysroot of the installation to operate on", "SYSROOT" },
		{ null }
	};

	construct {
		desc = "The carbonOS system update manager";
		arguments = "COMMAND";
		options = _options;
		subcommands = {
		    new StatusCommand(), // For system version & status info
		    new CheckCommand(), // Checks for updates
		    new DeployCommand(), // Installs updates
		    new SwitchCommand(), // Switches channels
		    new RollbackCommand(), // Un-deploys updates
		    new PinCommand(), // Pins/unpins deployments
		    new RebootCommand(), // Reboots into a deployment
		    new InitOSCommand(), // Initializes new OS
		    //new FactoryResetCommand(), // Factory reset
		    //new KargsCommand(), // Control kernel arguments
		};
		expected_args = 1; // Expect at least one subcommand
	}

	public override int main(string[] args) throws Error {
	    // Handle --version
		if (version) return print_version();

		// Populate opt_sysroot appropriately
		opt_sysroot = opt_sysroot ?? Environment.get_variable("OSTREE_SYSROOT");
		if (opt_sysroot != null)
		    opt_sysroot = Filename.canonicalize(opt_sysroot);
	    opt_sysroot = opt_sysroot ?? ""; // daemon expects "" instead of null

		// Pass on execution to subcommands
		return -1;
	}

	protected override bool check_enough_args() {
		return version; // If user is requesting version, don't need more args
	}

	private int print_version() throws Error {
	    var daemon = Daemon.get_main_service();
	    print("os-updated %s\n", daemon.version);
	    print("Configured for %s\n", daemon.os_name);
		return 0;
	}
}

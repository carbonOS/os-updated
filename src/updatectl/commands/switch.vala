using Cli;

class SwitchCommand : SubCommand {
	// Defines option parsing for the subcommand
	private static string collection;
	private static string base_img;
	private static string kernel;
	private const OptionEntry[] _options = {
		{ "collection", 'c', 0, OptionArg.STRING, ref collection, "The collection to pull updates from", "COLLECTION" },
		{ "base", 'b', 0, OptionArg.STRING, ref base_img, "The base image variant to use", "BASE" },
		{ "kernel", 'k', 0, OptionArg.STRING, ref kernel, "The kernel variant to use", "KERNEL" },
		{ null }
	};

	// Configure the subcommand
	construct {
		name = "switch";
		arguments = "[--collection/--kernel/--base]...";
		desc = "Select update channel and OS variant";
		options = _options;
		expected_args = 1;
		error_prefix = "Couldn't change update channel";
	}

	protected override bool check_enough_args() {
	    return collection != null || kernel != null || base_img != null;
	}

	// Entry point into the subcommand
	public override int main(string[] args) throws Error {
	    // Confirm changes
	    print("Changes:\n");
	    if (collection != null) print("  Collection -> \"%s\"\n", collection);
        if (base_img != null) print("  Base -> \"%s\"\n", base_img);
        if (kernel != null) print("  Kernel -> \"%s\"\n", kernel);
	    if (!prompt_yN("Are you sure you want to make these changes?")) {
	        print("Aborting.\n");
	        return 0;
	    }

	    var daemon = Daemon.get_main_service();
	    var progress = new Daemon.TaskOutput(daemon);

        // Ask the daemon to switch
	    bool done = false;
        daemon.switch_origin.begin(opt_sysroot, base_img ?? "<keep>",
            kernel ?? "<keep>", collection ?? "<keep>", (obj, res) => {
                try {
                    daemon.switch_origin.end(res);
                    done = true;
                } catch (Error e) {
                    progress.failed(e);
                }
            });
	    while (!done) progress.iteration();

        print("Done!\n");
        print(@"Run $(BOLD)systemctl reboot$(UNBOLD) to start a reboot\n");
		return 0;
	}
}

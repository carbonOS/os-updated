using Updated;

class ProgressImpl : Progress, DBusProxy {
    public ObjectPath object_path;
    private DBusConnection conn;
    private uint connection_id;

    public ProgressImpl(DBusConnection conn, string sender) {
        this.conn = conn;
        object_path = new ObjectPath("/sh/carbon/update1/%s".printf(
            sender.hash().to_string()));

        try {
            connection_id = conn.register_object<Progress>(object_path, this);
        } catch (Error e) {
            warning("Couldn't register progress for %s: %s", sender, e.message);
            connection_id = 0;
        }
    }

    public void deregister() {
        if (connection_id != 0)
            conn.unregister_object(connection_id);
    }
}

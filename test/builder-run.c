#include <unistd.h>
#include <stdlib.h>

int main() {
  chdir(SRCDIR);
  setenv("PREFIX", PREFIX, 1);
  setenv("LD_LIBRARY_PATH", PREFIX"/lib", 1);
  setenv("PATH", PREFIX"/bin:/usr/local/bin:/usr/bin", 1);
  execl(SRCDIR"/test/run", "test/run", NULL);
  return 1;
}

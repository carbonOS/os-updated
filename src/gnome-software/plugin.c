#define I_KNOW_THE_GNOME_SOFTWARE_API_IS_SUBJECT_TO_CHANGE
#include <glib.h>
#include <glib-object.h>
#include <gio/gio.h>
#include <gnome-software.h>
#include <os-updated.h>
#include "plugin.h"

/*
 * Breakdown of gs_app states that we cycle through
 * UNKNOWN -> no update available or unknown if update is available
 * UPDATABLE & size_download > 0 & !progress -> "ready to download"
 * UPDATABLE & size_download > 0 & progress -> "Downloading"
 * UPDATABLE & size_download == 0 & !progress -> "Ready to deploy"
 * UPDATABLE & size_download == 0 & progress -> "Deploying"
 * UPDATABLE -> "Waiting for reboot"
 */

struct _GsPluginOsUpdated {
  GsPlugin parent;

  const gchar *sysroot; // unowned
  GsApp *app; // thread-safe
  UpdatedService *proxy; // thread-safe

  gint64 last_check; // accessed in one function
  gboolean deployed; // doesn't matter if it's thread-safe
};
G_DEFINE_TYPE(GsPluginOsUpdated, gs_plugin_os_updated, GS_TYPE_PLUGIN)

/* HELPER FUNCTIONS */

// TODO: Translations
static inline const char*
_(const char* in)
{
  return in;
}

static void
error_convert(GError **perror)
{
  GError *error = (perror != NULL) ? *perror : NULL;

  // Cases we can bail early
  if (!error)
    return;
  if (error->domain == GS_PLUGIN_ERROR)
    return;

  // Error from updated
  if (g_dbus_error_is_remote_error(error)) {
    g_autofree gchar *remote = g_dbus_error_get_remote_error(error);
    g_dbus_error_strip_remote_error(error);
    error->domain = GS_PLUGIN_ERROR;

    if (g_str_equal(remote, "sh.carbon.update1.Error.Cancelled")) {
      error->code = GS_PLUGIN_ERROR_CANCELLED;
    } else if (g_str_equal(remote, "sh.carbon.update1.Error.AccessDenied")) {
      error->code = GS_PLUGIN_ERROR_AUTH_INVALID;
    } else if (g_str_equal(remote, "sh.carbon.update1.Error.NoRemotes") ||
               g_str_equal(remote, "sh.carbon.update1.Error.DownloadFailed")) {
      error->code = GS_PLUGIN_ERROR_DOWNLOAD_FAILED;
    } else if (g_str_equal(remote, "sh.carbon.update1.Error.MergeFailed") ||
               g_str_equal(remote, "sh.carbon.update1.Error.DeployFailed")) {
      error->code = GS_PLUGIN_ERROR_WRITE_FAILED;
    } else if (g_str_equal(remote, "sh.carbon.update1.Error.NoSpace")) {
      error->code = GS_PLUGIN_ERROR_NO_SPACE;
    } else if (g_str_equal(remote, "sh.carbon.update1.Error.LiveOs")) {
      error->code = GS_PLUGIN_ERROR_NOT_SUPPORTED;
    } else if (g_str_equal(remote, "sh.carbon.update1.Error.Failed") ||
               g_str_equal(remote, "sh.carbon.update1.Error.NoLock")) {
      error->code = GS_PLUGIN_ERROR_FAILED;
    } else {
      g_warning("Can't fixup remote error: %s", remote);
      error->code = GS_PLUGIN_ERROR_FAILED;
    }
    return;
  }

  // Lower-level errors
  if (gs_utils_error_convert_gio(perror))
    return;
  if (gs_utils_error_convert_gdbus(perror))
    return;
}

static void
reset_app_state(GsPluginOsUpdated *self)
{
  GsAppState old_app_state = gs_app_get_state(self->app);

  // Reset the app's state
  gs_app_set_state(self->app, GS_APP_STATE_UNKNOWN);
  gs_app_set_size_download(self->app, GS_SIZE_TYPE_UNKNOWN, 0);
  gs_app_set_update_version(self->app, NULL);
  gs_app_set_update_details_markup(self->app, NULL);

  // Notify the frontend if the state changed
  if (old_app_state != GS_APP_STATE_UNKNOWN)
    gs_plugin_updates_changed(GS_PLUGIN(self));

  // Forget a past successful deployment
  self->deployed = FALSE;
}

static gboolean
propagate_error(GsPluginOsUpdated *self,
                GError **error,
                GError **local_error)
{
  // Sanity check
  if (*local_error == NULL) {
    g_warning("Tried to propagate null error!");
    return TRUE;
  }

  reset_app_state(self);
  error_convert(local_error);
  g_propagate_error(error, g_steal_pointer(local_error));
  return FALSE;
}

static void
cancelled_cb(GCancellable *c, gpointer user_data)
{
  GsPluginOsUpdated *self = GS_PLUGIN_OS_UPDATED(user_data);
  g_autoptr(GError) local_error = NULL;

  // Ask the daemon to cancel
  updated_service_cancel(self->proxy, &local_error);

  // We can't really do much in the event of an error...
  if (g_dbus_error_is_remote_error(local_error))
    g_dbus_error_strip_remote_error(local_error);
  if (local_error != NULL)
    g_critical("Failed to cancel! %s", local_error->message);
}

static gulong
chain_cancellable(GsPluginOsUpdated *self, GCancellable *cancellable)
{
  gulong cancelled_id = 0;
  if (cancellable != NULL) {
    cancelled_id = g_cancellable_connect(cancellable,
                                         G_CALLBACK(cancelled_cb),
                                         self,
                                         NULL);
  }
  return cancelled_id;
}

/* SETUP */

static void
gs_plugin_os_updated_init(GsPluginOsUpdated *self)
{
  // Initialize path to sysroot
  self->sysroot = g_getenv("OSTREE_SYSROOT");
  if (!self->sysroot) self->sysroot = "";

  // Disable the plugin if not booted with ostree
  if (!g_file_test("/run/ostree-booted", G_FILE_TEST_EXISTS))
    gs_plugin_set_enabled(GS_PLUGIN(self), FALSE);
}

static void
proxy_new_cb(GObject *source_object,
             GAsyncResult *result,
             gpointer user_data);

static void
gs_plugin_os_updated_setup_async(GsPlugin *plugin,
                                 GCancellable *cancellable,
                                 GAsyncReadyCallback callback,
                                 gpointer user_data)
{
  GsPluginOsUpdated *self = GS_PLUGIN_OS_UPDATED(plugin);
  g_autoptr(GTask) task = NULL;
  g_autoptr(GsApp) app = NULL;
  g_autoptr(GIcon) app_icon = NULL;
  g_autofree gchar *os_version = NULL;

  task = g_task_new(plugin, cancellable, callback, user_data);
  g_task_set_source_tag(task, gs_plugin_os_updated_setup_async);

  // Create the app
  app = gs_app_new("sh.carbon.os-updated.Update");
  app_icon = g_themed_icon_new("system-component-os-updates");
  gs_app_add_icon(app, app_icon);
  gs_app_set_special_kind(app, GS_APP_SPECIAL_KIND_OS_UPDATE);
  gs_app_set_scope(app, AS_COMPONENT_SCOPE_SYSTEM);
  gs_app_set_kind(app, AS_COMPONENT_KIND_OPERATING_SYSTEM);
  gs_app_set_name(app, GS_APP_QUALITY_NORMAL, _("System Updates"));
  gs_app_set_summary(app, GS_APP_QUALITY_NORMAL,
                     _("Includes new features, bug fixes, and other improvements"));
  os_version = g_get_os_info(G_OS_INFO_KEY_VERSION_ID);
  gs_app_set_version(app, os_version);
  gs_app_add_quirk(app, GS_APP_QUIRK_NEEDS_REBOOT);
  gs_app_add_quirk(app, GS_APP_QUIRK_PROVENANCE);
  gs_app_add_quirk(app, GS_APP_QUIRK_NOT_REVIEWABLE);
  gs_app_add_quirk(app, GS_APP_QUIRK_COMPULSORY);
  gs_app_set_allow_cancel(app, TRUE);
  gs_app_set_management_plugin(app, GS_PLUGIN(self));
  self->app = g_steal_pointer(&app);

  // Create a proxy for the service
  updated_service_new_async(cancellable, proxy_new_cb, g_steal_pointer(&task));
}

static void
proxy_new_cb(GObject *source_object,
             GAsyncResult *result,
             gpointer user_data)
{
  g_autoptr(GTask) task = g_steal_pointer(&user_data);
  GsPluginOsUpdated *self = g_task_get_source_object(task);
  g_autoptr(GError) local_error = NULL;

  self->proxy = updated_service_new_finish(result, &local_error);
  if (!self->proxy) {
    error_convert(&local_error);
    g_task_return_error(task, g_steal_pointer(&local_error));
    return;
  }

  g_debug("Initialized");

  // Return success
  g_task_return_boolean(task, TRUE);
}

static gboolean
gs_plugin_os_updated_setup_finish(GsPlugin *plugin,
                                  GAsyncResult *result,
                                  GError **error)
{
  return g_task_propagate_boolean(G_TASK(result), error);
}

/* PROGRESS TRACKING */

static void
on_progress_pulse(GsPluginOsUpdated *self)
{
  g_debug(G_STRFUNC);
  gs_app_set_progress(self->app, -1);
}

static void
on_progress_coarse(GsPluginOsUpdated *self, guint done, guint total)
{
  g_debug("%s(%u, %u)", G_STRFUNC, done, total);

  gdouble percent = (gdouble) done / (gdouble) total;
  gs_app_set_progress(self->app, (guint)(percent * 100));
  gs_app_set_size_download(self->app, GS_SIZE_TYPE_UNKNOWABLE, 0);
}

static void
on_progress_fine(GsPluginOsUpdated *self,
                 guint64 done, guint64 total,
                 UpdatedProgress *progress)
{
  g_debug("%s(%lu, %lu)", G_STRFUNC, done, total);

  gdouble percent = (gdouble) done / (gdouble) total;
  gs_app_set_progress(self->app, (guint)(percent * 100));
  gs_app_set_size_download(self->app, GS_SIZE_TYPE_VALID, total);

  // Once we have fine progress, stop listening for coarse
  g_signal_handlers_disconnect_by_func(progress, G_CALLBACK(on_progress_coarse),
                                       self);
}

static void
on_progress_finished(GsPluginOsUpdated *self)
{
  g_debug(G_STRFUNC);
  gs_app_set_progress(self->app, 0);
}

static UpdatedProgress*
connect_progress(GsPluginOsUpdated *self,
                 GCancellable *cancellable,
                 GError **error)
{
  UpdatedProgress *progress = NULL;

  // Get the progress
  progress = updated_service_get_progress(self->proxy, cancellable, error);
  if (!progress) {
    reset_app_state(self);
    error_convert(error);
    return NULL;
  }

  // Connect signals
  // NOTE: These signals auto-disconnect when the progress object is finalized
  g_signal_connect_object(progress, "resolving",
                          G_CALLBACK(on_progress_pulse),
                          self, G_CONNECT_SWAPPED);
  g_signal_connect_object(progress, "pulling",
                          G_CALLBACK(on_progress_pulse),
                          self, G_CONNECT_SWAPPED);
  g_signal_connect_object(progress, "downloaded",
                          G_CALLBACK(on_progress_fine),
                          self, G_CONNECT_SWAPPED);
  g_signal_connect_object(progress, "downloaded-coarse",
                          G_CALLBACK(on_progress_coarse),
                          self, G_CONNECT_SWAPPED);
  g_signal_connect_object(progress, "merging",
                          G_CALLBACK(on_progress_pulse),
                          self, G_CONNECT_SWAPPED);
  g_signal_connect_object(progress, "deploying",
                          G_CALLBACK(on_progress_pulse),
                          self, G_CONNECT_SWAPPED);
  g_signal_connect_object(progress, "pruning",
                          G_CALLBACK(on_progress_pulse),
                          self, G_CONNECT_SWAPPED);
  g_signal_connect_object(progress, "finished",
                          G_CALLBACK(on_progress_finished),
                          self, G_CONNECT_SWAPPED);

  // Transfer ownership to caller
  return progress;
}

/* MAIN FUNCTIONALITY */

gboolean
gs_plugin_add_updates(GsPlugin *plugin,
                      GsAppList *list,
                      GCancellable *cancellable,
                      GError **error)
{
  GsPluginOsUpdated *self = GS_PLUGIN_OS_UPDATED(plugin);
  if (gs_app_get_state(self->app) == GS_APP_STATE_UPDATABLE)
	  gs_app_list_add(list, self->app);
  return TRUE;
}

static void
check_cb(GObject *source_object,
         GAsyncResult *result,
         gpointer user_data);

static void
gs_plugin_os_updated_refresh_metadata_async(GsPlugin *plugin,
                                            guint64 cache_age_secs,
                                            GsPluginRefreshMetadataFlags flags,
                                            GCancellable *cancellable,
                                            GAsyncReadyCallback callback,
                                            gpointer user_data)
{
  GsPluginOsUpdated *self = GS_PLUGIN_OS_UPDATED(plugin);
  g_autoptr(GTask) task = NULL;
  gulong cancelled_id = 0;

  // Set up the GTask
  task = g_task_new(plugin, cancellable, callback, user_data);
  g_task_set_source_tag(task, gs_plugin_os_updated_refresh_metadata_async);

  // Bail if we've refreshed recently
  if (g_get_monotonic_time() - self->last_check < (cache_age_secs * 1000000)) {
    g_task_return_boolean(task, TRUE);
    return;
  }

  g_debug(G_STRFUNC);

  // Chain the cancellable
  cancelled_id = chain_cancellable(self, cancellable);
  g_object_set_data(G_OBJECT(task), "cid", GUINT_TO_POINTER(cancelled_id));

  // Ask daemon to check for updates
  updated_service_check (self->proxy, self->sysroot, check_cb,
                         g_steal_pointer(&task));
}

static void
check_cb(GObject *source_object,
         GAsyncResult *async_res,
         gpointer user_data)
{
  g_autoptr(GTask) task = g_steal_pointer(&user_data);
  GsPluginOsUpdated *self = g_task_get_source_object(task);
  g_autoptr(UpdatedAutoReleaser) releaser = NULL;
  g_auto (UpdatedCheckResult) result = { 0 };
  g_autoptr(GError) local_error = NULL;
  gulong cancelled_id = 0;

  // Automatically release the service when we're done
  releaser = updated_auto_releaser_new(self->proxy);

  // Finish async call
  updated_service_check_finish (self->proxy, async_res, &result, &local_error);

  // Unchain cancellable
  cancelled_id = GPOINTER_TO_UINT(g_object_get_data(G_OBJECT(task), "cid"));
  g_cancellable_disconnect(g_task_get_cancellable(task), cancelled_id);

  // Bail if the daemon failed
  if (local_error != NULL) {
    reset_app_state(self);
    error_convert(&local_error);
    g_task_return_error(task, g_steal_pointer(&local_error));
    return;
  }

  // Store the timestamp of our last successful check
  self->last_check = g_get_monotonic_time();

  // Force a re-deploy
  self->deployed = FALSE;

  // Update the state of the app
  if (updated_update_availability_to_bool(result.availability)) {
    g_debug("Update found");

    GsAppState old_state = gs_app_get_state(self->app);
    gs_app_set_state(self->app, GS_APP_STATE_UPDATABLE);

    gs_app_set_size_download(self->app, GS_SIZE_TYPE_VALID, result.download_size);

    if (g_str_equal(result.new_version, "none"))
      gs_app_set_update_version(self->app, NULL);
    else
      gs_app_set_update_version(self->app, result.new_version);

    if (old_state != GS_APP_STATE_UPDATABLE)
      gs_plugin_updates_changed(GS_PLUGIN(self));
  } else {
    g_debug("No update available");
    reset_app_state(self);
  }

  g_task_return_boolean(task, TRUE);
}

static gboolean
gs_plugin_os_updated_refresh_metadata_finish(GsPlugin *plugin,
                                             GAsyncResult *result,
                                             GError **error)
{
  return g_task_propagate_boolean(G_TASK(result), error);
}

static gboolean
do_pull(GsPluginOsUpdated *self,
        GCancellable *cancellable,
        GError **error)
{
  g_autoptr(GError) local_error = NULL;
  gpointer metered_handle = NULL;
  gulong cancelled_id = 0;
  guint64 dl_size;

  // Bail if we have nothing to download
  if (gs_app_get_size_download(self->app, &dl_size) != GS_SIZE_TYPE_VALID) {
      g_set_error(error, GS_PLUGIN_ERROR, GS_PLUGIN_ERROR_DOWNLOAD_FAILED,
                  "Cannot determine if update is already downloaded");
      return FALSE;
  }
  if (dl_size == 0)
    return TRUE;

  g_debug(G_STRFUNC);

  // Wait for the download scheduler if appropriate
  if (!gs_plugin_has_flags(GS_PLUGIN(self), GS_PLUGIN_FLAGS_INTERACTIVE)) {
    GVariant *params = gs_metered_build_scheduler_parameters_for_app(self->app);
    gboolean success = gs_metered_block_on_download_scheduler(params,
                                                              &metered_handle,
                                                              cancellable,
                                                              error);
    if (!success)
      return FALSE;
  }

  // Ask the daemon to do the download
  cancelled_id = chain_cancellable(self, cancellable);
  updated_service_pull_sync(self->proxy, self->sysroot, error);
  g_cancellable_disconnect(cancellable, cancelled_id);

  // Remove scheduler entry
  if (!gs_metered_remove_from_download_scheduler(metered_handle, cancellable,
                                                 &local_error)) {
    g_warning("Failed to remove schedule entry: %s", local_error->message);
    g_clear_error(&local_error);
  }

  // Bail if the daemon failed
  if (*error != NULL)
    return FALSE;

  // Mark that there's nothing left to download
  gs_app_set_size_download(self->app, GS_SIZE_TYPE_VALID, 0);

  return TRUE;
}

static gboolean
do_deploy(GsPluginOsUpdated *self,
          GCancellable *cancellable,
          GError **error)
{
  gulong cancelled_id = 0;
  guint64 dl_size;

  // Skip if we've already deployed this update
  if (self->deployed)
    return TRUE;

  // Bail if there's something left to download
  if (gs_app_get_size_download(self->app, &dl_size) != GS_SIZE_TYPE_VALID) {
    g_set_error(error, GS_PLUGIN_ERROR, GS_PLUGIN_ERROR_WRITE_FAILED,
                      "Cannot determine if update finished downloading");
    return FALSE;
  }
  if (dl_size != 0) {
      g_set_error(error, GS_PLUGIN_ERROR, GS_PLUGIN_ERROR_WRITE_FAILED,
                  "Update didn't finish downloading");
      return FALSE;
  }

  g_debug(G_STRFUNC);

  // Ask the daemon to deploy the update
  cancelled_id = chain_cancellable(self, cancellable);
  updated_service_deploy_sync(self->proxy, self->sysroot, error);
  g_cancellable_disconnect(cancellable, cancelled_id);

  // Bail if the daemon failed
  if (*error != NULL)
    return FALSE;

  // Don't try to re-deploy this update
  self->deployed = TRUE;
  g_debug("Update deployed!");

  return TRUE;
}

gboolean
gs_plugin_download_app(GsPlugin *plugin,
                       GsApp *app,
                       GCancellable *cancellable,
                       GError **error)
{
  GsPluginOsUpdated *self = GS_PLUGIN_OS_UPDATED(plugin);
  g_autoptr(GError) local_error = NULL;
  g_autoptr(UpdatedProgress) progress = NULL;
  g_autoptr(UpdatedAutoReleaser) releaser = NULL;

  g_debug(G_STRFUNC);

  releaser = updated_auto_releaser_new(self->proxy);

  progress = connect_progress(self, cancellable, &local_error);
  if (!progress)
    return propagate_error(self, error, &local_error);

  if (!do_pull(self, cancellable, &local_error))
    return propagate_error(self, error, &local_error);

  if (!do_deploy(self, cancellable, &local_error))
    return propagate_error(self, error, &local_error);

  return TRUE;
}

gboolean
gs_plugin_update_app(GsPlugin *plugin,
                     GsApp *app,
                     GCancellable *cancellable,
                     GError **error)
{
  GsPluginOsUpdated *self = GS_PLUGIN_OS_UPDATED(plugin);
  g_autoptr(GError) local_error = NULL;
  g_autoptr(UpdatedProgress) progress = NULL;
  g_autoptr(UpdatedAutoReleaser) releaser = NULL;

  g_debug(G_STRFUNC);

  releaser = updated_auto_releaser_new(self->proxy);

  progress = connect_progress(self, cancellable, &local_error);
  if (!progress)
    return propagate_error(self, error, &local_error);

  if (!do_deploy(self, cancellable, &local_error))
    return propagate_error(self, error, &local_error);

  return TRUE;
}

/* TEARDOWN */

static void
gs_plugin_os_updated_dispose(GObject *object)
{
  GsPluginOsUpdated *self = GS_PLUGIN_OS_UPDATED(object);

  g_clear_object(&self->app);

  if (self->proxy) updated_service_release(self->proxy, NULL);
  g_clear_object(&self->proxy);

  G_OBJECT_CLASS(gs_plugin_os_updated_parent_class)->dispose(object);
}

static void
gs_plugin_os_updated_class_init(GsPluginOsUpdatedClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS(klass);
  GsPluginClass *plugin_class = GS_PLUGIN_CLASS(klass);

  object_class->dispose = gs_plugin_os_updated_dispose;

  plugin_class->setup_async = gs_plugin_os_updated_setup_async;
  plugin_class->setup_finish = gs_plugin_os_updated_setup_finish;
  plugin_class->refresh_metadata_async = gs_plugin_os_updated_refresh_metadata_async;
  plugin_class->refresh_metadata_finish = gs_plugin_os_updated_refresh_metadata_finish;
}

GType
gs_plugin_query_type(void)
{
  return GS_TYPE_PLUGIN_OS_UPDATED;
}


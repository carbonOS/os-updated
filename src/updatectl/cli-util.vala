namespace Cli {
	// REFERENCE: https://misc.flogisoft.com/bash/tip_colors_and_formatting

	public const string BOLD = "\x1b[1m";
	public const string DIM = "\x1b[2m";
	public const string UL = "\x1b[1m";

	// FOREGROUND
	public const string BLACK = "\x1b[30m";
	public const string RED = "\x1b[31m";
	public const string GREEN = "\x1b[32m";
	public const string YELLOW = "\x1b[33m";
	public const string BLUE = "\x1b[34m";
	public const string MAGENTA = "\x1b[35m";
	public const string CYAN = "\x1b[36m";
	public const string LGRAY = "\x1b[37m";
	public const string DGRAY = "\x1b[90m";

	// BACKGROUND
	public const string BACK_BLACK = "\x1b[40m";
	public const string BACK_RED = "\x1b[41m";
	public const string BACK_GREEN = "\x1b[42m";
	public const string BACK_YELLOW = "\x1b[43m";
	public const string BACK_BLUE = "\x1b[44m";
	public const string BACK_MAGENTA = "\x1b[45m";
	public const string BACK_CYAN = "\x1b[46m";
	public const string BACK_LGRAY = "\x1b[47m";
	public const string BACK_DGRAY = "\x1b[100m";
	public const string BACK_NEUTRAL_GRAY = "\x1b[48;2;128;128;128m";

	// RESET
	public const string RESET = "\x1b[0m";
	public const string UNBOLD = "\x1b[22m";
	public const string UN_UL = "\x1b[24m";
	public const string RESET_COLOR = "\x1b[39m";
	public const string RESET_BACK = "\x1b[49m";
	public const string CLR_LINE = "\x1b[G\x1b[K";

    // Spinners
    public const string[] UTF8_SPINNERS = {
        "\u280F", // ⠏
        "\u281B", // ⠛
        "\u2839", // ⠹
        "\u28B8", // ⢸
        "\u28F0", // ⣰
        "\u28E4", // ⣤
        "\u28C6", // ⣆
        "\u2847" // ⡇
    };
    public const string[] ASCII_SPINNERS = { "|", "/", "-", "\\" };

	// Special Glyphs
    // Reference: https://github.com/systemd/systemd/blob/main/src/basic/locale-util.c#L340
	enum Glyph {
        DOT, SPINNER, PASS, FAIL, PROGRESS_BLOCK, PROGRESS_START, PROGRESS_END;

	    public string to_string(int frame = -1) {
	        var utf8 = get_charset(null);
	        switch (this) {
	            case DOT:
	                return utf8 ? "\u25cf" : "*"; // ●
	            case SPINNER:
	                if (frame == -1) assert_not_reached();
	                return utf8 ? UTF8_SPINNERS[frame] : ASCII_SPINNERS[frame % 4];
	            case PASS:
	                return utf8 ? "\u2713" : "+"; // ✓
	            case FAIL:
	                return utf8 ? "\u2717" : "-"; // ✗ (BALLOT X)
	            case PROGRESS_BLOCK:
	                return utf8 ? "\u2588" : "="; // █ (FULL BLOCK)
	            case PROGRESS_START:
	                return utf8 ? "" : "[";
	            case PROGRESS_END:
	                return utf8 ? "" : "]";
	            default:
	                assert_not_reached();
	        }
	    }
	}

    // Prompt
    public static bool assume_yes = false;
	public bool prompt_yN(string message) {
	    if (assume_yes) return true;

		print("%s [y/N]: ", message);
		var line = stdin.read_line();
		switch (line.down()) {
			case "y":
			case "ye":
			case "yep":
			case "yes":
			case "ayy":
			case "go ahead":
			case "mmhmm":
			case "why not":
			case "do it":
			case "sure":
			case "ok":
			case "okay":
				return true;
			default:
				return false;
		}
	}

	// Progress bar
	public const string[] FRACTIONAL_BLOCKS = {
	    "\u258F", "\u258E", "\u258D", "\u258C", "\u258B", "\u258A", "\u2589", "\u2588"
	};
	public string progress_bar(double percent, int length = 20) {
	    if (length <= 0) return "";
	    var fill = percent * length;

        var utf8 = get_charset(null);
	    string bar = "";
	    if (utf8) bar += BACK_NEUTRAL_GRAY;
	    var i = 0;
	    for (; i < fill; i++) {
            var frac = fill - i;
            if (frac > 1)
                bar += Glyph.PROGRESS_BLOCK.to_string();
            else if (!utf8)
                bar += ">";
            else {
                string block = ""; // Calculate a partial block
                for (int j = 0; j < 8; j++)
                    if (frac >= j / 8.0) block = FRACTIONAL_BLOCKS[j];
                bar += block;
            }
	    }
	    for (; i < length; i++) bar += " ";
	    if (utf8) bar += RESET_BACK;
	    return @"$(Glyph.PROGRESS_START)%s$(Glyph.PROGRESS_END) ".printf(bar);
	}

	// Narrow mode
	public int get_term_width() {
        Linux.winsize size;
        if (Linux.ioctl(Posix.STDIN_FILENO, Linux.Termios.TIOCGWINSZ, out size) != 0)
            return 80;

        return size.ws_col;
	}
}

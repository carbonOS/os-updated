using Cli;
using Updated;

class CheckCommand : SubCommand {
	// Defines option parsing for the subcommand
	private static bool download;
	private const OptionEntry[] _options = {
		{ "download", 'd', 0, OptionArg.NONE, ref download, "Download updates if any are available", null },
		{ null }
	};

	// Configure the subcommand
	construct {
		name = "check";
		desc = "Check if any updates are available for download";
		options = _options;
		error_prefix = "Failed to check for updates";
	}

	// Entry point into the subcommand
	public override int main (string[] args) throws Error {
	    var daemon = Daemon.get_main_service();
	    var progress = new Daemon.TaskOutput(daemon);

        // Ask the daemon to check for updates
        progress.pulling_msg = "Fetching metadata";
        CheckResult? result = null;
	    daemon.check.begin(opt_sysroot, (obj, res) => {
	        try {
	            result = daemon.check.end(res);
	        } catch (Error e) {
	            progress.failed(e);
	        }
	    });
        while (result == null) progress.iteration();

        // Print out the result
        var status = result.availability;
        if (status == UpdateAvailability.NONE)
            print("No updates available.\n");
        else {
            string[] updates = {};
            if (UpdateAvailability.BASE in status)
                updates += "base";
            if (UpdateAvailability.KERNEL in status)
                updates += "kernel";
            var updates_str = string.joinv(", ", updates);

            print("Updates available: %s\n", updates_str);
        }

        // Print size info & prompt to continue download if appropriate
        if (result.download_size != -1) {
            if (result.download_size == 0) {
                print("Updates are already downloaded\n");
                return 0;
            }

            var formatted_size = format_size(result.download_size);
            print("Estimated download size: %s\n", formatted_size);

            if (download && !prompt_yN("Continue with download?")) {
                print("Aborted.\n");
                return 0;
            }
        }

        if (download) {
            progress.resolving_msg = null;
            progress.pulling_msg = "Downloading";

            var done = false;
            daemon.pull.begin(opt_sysroot, (obj, res) => {
                try {
                    daemon.pull.end(res);
                    done = true;
                } catch (Error e) {
                    progress.failed(e);
                }
            });
            while (!done) progress.iteration();
        }

		return 0;
	}
}

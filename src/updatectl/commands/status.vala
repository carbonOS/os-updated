using Cli;

class StatusCommand : SubCommand {
    const string LBL = BOLD + "%s:" + RESET;
    const string FIELD = LBL + " %s\n" + RESET;
    const string INDENT = "    ";
    const string MISSING = YELLOW + BOLD + "UNKNOWN" + RESET;

	construct {
		name = "status";
		aliases = { "s" };
		desc = "Show system update status";
	}

	public override int main(string[] args) throws Error {
	    var term_w = get_term_width();
	    var daemon = Daemon.get_main_service();
        bool is_booted;
        var deployments = daemon.get_deployments(opt_sysroot, out is_booted);

        if (is_booted) {
            var pretty = Environment.get_os_info("PRETTY_NAME") ?? "Unknown";
            var variant = Environment.get_os_info("VARIANT_ID") ?? "Unknown";
            var os_info = Posix.utsname(); // calls uname()

            var os_str = "%s (%s)".printf(pretty, variant);
            var kernel_str = "%s (%s)".printf(os_info.release, os_info.machine);

            print(FIELD, "Booted OS", os_str);
            print(FIELD, "Booted Kernel", kernel_str);
            print("\n");
        }

        print(FIELD, "Deployments", "");
        foreach (var it in deployments) {
            // Print header
            var hdr_boot = it.is_booted ? @"$GREEN$(Glyph.DOT)$RESET" : " ";
            var hdr_status = "";
            if (it.is_staged)
                hdr_status = " (staged)";
            else if (it.is_pending)
                hdr_status = " (pending)";
            else if (it.is_rollback)
                hdr_status = " (rollback)";
            if (it.is_pinned)
                hdr_status += " (pinned)";
            if (it.is_pending_delete) // Waiting for reboot to be deleted
                hdr_status += " (pending deletion)";
            print(@"%s $BOLD%s %s$RESET%s$RESET\n", hdr_boot, it.os_name,
                it.metadata["version"] ?? MISSING, hdr_status);

            // Print checksum + deployment serial, & our checksums
            var deployment_csum = (term_w < 90) ?
                it.csum.substring(0, 20) + "]" : it.csum;
            var csum_disp = "%s.%d".printf(deployment_csum, it.deploy_serial);
            print(INDENT + FIELD, "Checksum", csum_disp);
            var base_csum = it.metadata["base_csum"] ?? MISSING;
            if (term_w < 90) base_csum = base_csum.substring(0, 20) + "]";
            var base_variant = it.metadata["base_variant"] ?? MISSING;
            var base_str = "%s (%s)".printf(base_csum, base_variant);
            print(INDENT + FIELD, "Base", base_str);
            var kernel_csum = it.metadata["kernel_csum"] ?? MISSING;
            if (term_w < 90) kernel_csum = kernel_csum.substring(0, 20) + "]";
            var kernel_variant = it.metadata["kernel_variant"] ?? MISSING;
            var kernel_str = "%s (%s)".printf(kernel_csum, kernel_variant);
            print(INDENT + FIELD, "Kernel", kernel_str);

            // Print timestamp
            var timestamp_dt = new DateTime.from_unix_local((int64) it.timestamp);
            var timestamp_str = timestamp_dt.format("%Y-%m-%d %H:%M:%S");
            print(INDENT + FIELD, "Timestamp", timestamp_str);

            // Print remote
            var remote = it.metadata["collection"] ?? @"$YELLOW$(BOLD)none$RESET";
            print(INDENT + FIELD, "Collection", remote);

            // Print kernel version
            var linux_ver = it.metadata["linux"] ?? MISSING;
            print(INDENT + FIELD, "Linux", linux_ver);

            // Print unlock state
            var unlock_state = it.unlock_state;
            var unlock_str = "no";
            if (unlock_state != OSTree.DeploymentUnlockedState.NONE)
                unlock_str = @"$RED$BOLD%s$RESET".printf(unlock_state.to_string());
            print(INDENT + FIELD, "Unlocked", unlock_str);
        }

        return 0;
    }
}

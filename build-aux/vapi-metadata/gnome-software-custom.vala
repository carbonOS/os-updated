namespace Gs {
    class Plugin : GLib.Object {
        public void* alloc_data(size_t size);
        public void* get_data();
    }

    public static bool utils_error_convert_gio(GLib.Error** perror);
    public static bool utils_error_convert_gdbus(GLib.Error** perror);
}

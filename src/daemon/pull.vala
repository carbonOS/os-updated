using Updated;

async void pull_impl(OSTree.Sysroot sysroot, KeyFile? origin_override,
        Progress progress, Cancellable cancellable) throws UpdateError {
    Utils.bail_if_cancelled(cancellable);
    var merge = sysroot.get_merge_deployment(OS_NAME);
    var repo = sysroot.repo();
    OTUtils.ensure_writable(repo);

    // Parse origin to get collection refs
    var origin = origin_override ?? OTUtils.get_origin(merge);
    OSTree.CollectionRef base_cref, kernel_cref;
    OTUtils.parse_origin(origin, out base_cref, out kernel_cref);

    // Find remotes to pull from
    progress.resolving();
    OSTree.RepoFinderResult[] remotes;
    try {
        remotes = yield repo.find_remotes_async({base_cref, kernel_cref},
            null, null, null, cancellable);
    } catch (Error e) {
        Utils.bail_if_cancelled(cancellable);
        throw new UpdateError.NO_REMOTES(e.message);
    }
    progress.finished();

    // Bail if we don't have any remotes available
    if (remotes.length == 0)
        throw new UpdateError.NO_REMOTES("No remotes found");

    Utils.bail_if_cancelled(cancellable);

    // Set up the progress reporting
    var ostree_progress = new OSTree.AsyncProgress();
    ostree_progress.changed.connect(() => {
        uint64 transferred_bytes, total_bytes;
        uint transferred_objs, total_objs, scanning, outstanding_meta;
        ostree_progress.@get(
            "bytes-transferred", "t", out transferred_bytes,
            "total-delta-part-size", "t", out total_bytes,
            "fetched", "u", out transferred_objs,
            "requested", "u", out total_objs,
            "scanning", "u", out scanning,
            "outstanding-metadata-fetches", "u", out outstanding_meta
        );

        // Report coarse download information
        if (likely(total_objs != 0)) { // We should have it basically always
            // Only report coarse progress if we're no longer estimating
            // the total number of objects
            if (scanning == 0 && outstanding_meta == 0)
                progress.downloaded_coarse(transferred_objs, total_objs);
        }

        // Bail if we don't have fine size information
        if (total_bytes == 0) return;

        // Increase the total size by 1%. Helps deal with the extra
        // non-delta content that OSTree will download. Also helps prevent
        // the progress bar from sitting at 100% as the download finishes up
        total_bytes = (uint64)(total_bytes * 1.01);

        // It's possible the boost wasn't enough. Make sure we never report
        // more transferred bytes than total bytes
        if (unlikely(transferred_bytes > total_bytes))
            total_bytes = transferred_bytes;

        // Send the report
        progress.downloaded(transferred_bytes, total_bytes);
    });
    progress.pulling();

    // Do the pull
    try {
        yield repo.pull_from_remotes_async(remotes, null, ostree_progress,
            cancellable);
    } catch (Error e) {
        Utils.bail_if_cancelled(cancellable);
        throw new UpdateError.DOWNLOAD_FAILED(e.message);
    }

    // Notify that we're done
    ostree_progress.finish();
    progress.finished();
}

using Updated;
using Cli;

class PinCommand : SubCommand {
	// Defines option parsing for the subcommand
	private static bool opt_unpin;
	private const OptionEntry[] _options = {
		{ "unpin", 'u', 0, OptionArg.NONE, ref opt_unpin, "Unpin the deployment (alias: updatectl unpin)", null },
		{ null }
	};

	// Configure the subcommand
	construct {
		name = "pin";
		aliases = { "unpin" };
		desc = "Pin/unpin an OS deployment";
		arguments = "DEPLOYMENT";
		expected_args = 1;
		options = _options;
		error_prefix = "Failed to pin deployment";
	}

	// Entry point into the subcommand
	public override int main(string[] args) throws Error {
	    // Handle unpin alias
	    if (args[0].has_suffix("unpin")) opt_unpin = true;
	    if (opt_unpin) error_prefix = "Failed to unpin deployment";

	    // Get the deployments from the daemon
	    var daemon = Daemon.get_main_service();
	    bool is_booted; // Only need this to prevent a segfault
        var deployments = daemon.get_deployments(opt_sysroot, out is_booted);

        // Parse out the deployment we're interested in
        var idx = OTUtil.parse_deployment_name(deployments, args[1]);
        var deployment = deployments[idx];

        // Error out if the deployment is staged
        if (deployment.is_staged)
            throw new UpdateError.FAILED("Can't pin staged deployments");

        // Error out if it's already in the state we want it to be
        if (deployment.is_pinned && !opt_unpin)
            throw new UpdateError.FAILED("Deployment is already pinned");
        else if (!deployment.is_pinned && opt_unpin)
            throw new UpdateError.FAILED("Deployment is already unpinned");

        // Ask the daemon to set pin status
        bool done = false;
        Error? daemon_error = null;
        daemon.set_pinned.begin(opt_sysroot, idx, !opt_unpin, (obj, res) => {
            done = true;
            try {
                daemon.set_pinned.end(res);
            } catch (Error e) {
                daemon_error = e;
            }
        });
        while (!done) MainContext.@default().iteration(false);
        if (daemon_error != null) throw daemon_error;

		return 0;
	}
}

using Cli;

abstract class SubCommand : GLib.Object {
	public string name = "UNSET"; // Main name for this subcommand
	public string desc = ""; // Description for this subcommand
	public string arguments = ""; // Help representation of the options
	public string[] aliases = {}; // Alternate names for this subcommand
	public OptionEntry[]? options = null; // Options for OptionContext
	public SubCommand[]? subcommands = null; // List of subcommands
    public string? error_prefix = null; // Prefix for errors

	protected int help_length = 22; // The offset at which to print desc in help
	protected int expected_args = 0; // How many non-flag args are required

	public int invoke(string[] args) {
        //for(int i = 0;i<args.length;i++)debug("COMMAND=%s, args[%d]=%s",name,i,args[i]);

        var help_mode = "--help" in args || "-h" in args || "-?" in args;
        var help_text = "";

		try {
			Environment.set_prgname(args[0]);
			var parser = new OptionContext(arguments);
			if (desc != "") parser.set_summary(desc);
			if (options != null) parser.add_main_entries(options, null);

			// Deal with subcommands
			if (subcommands != null && subcommands.length > 0) {
                // Bind --help to the subcommand that precedes it
			    parser.set_strict_posix(help_mode);
			    parser.set_ignore_unknown_options(true);

				// Generate custom help section
				string commands = "Available subcommands:\n";
				foreach (var subcommand in subcommands)
					commands += "  %-*s%s\n".printf(help_length, subcommand.name, subcommand.desc);
				parser.set_description(commands);

                // Store the help text
				help_text = parser.get_help(true, null);
			}

			parser.parse(ref args);
		} catch (OptionError e) {
			return print_opt_error(e.message, args);
		}

		if (args.length <= expected_args && !check_enough_args())
			return not_enough_arguments_error(args);

		int main_status;
		try { // Invoke the main command
		    main_status = main(args);
		} catch (Error e) {
		    DBusError.strip_remote_error(e);
		    return print_error((error_prefix ?? "") +
		        (error_prefix != null ? @":$UNBOLD " : "") + e.message);
		}
        if (main_status != -1) return main_status;
        // Subcommands take control if main command returns -1

        if (args.length == 1) // No args[1] to interpret as a subcommand
            not_enough_arguments_error(args);

        // Find the subcommand
        foreach (var subcommand in subcommands)
            if (args[1] == subcommand.name || args[1] in subcommand.aliases) {
                // Create a new args[] for the subcommand w/ a special args[0]
                string[] subcommand_args = {};
                subcommand_args += "%s %s".printf(args[0], args[1]);
                for (int i = 2; i < args.length; i++) subcommand_args += args[i];

                // Execute the subcommand
                return subcommand.invoke(subcommand_args);
            }

        // We couldn't find a subcommand
        if (!help_mode)
		    return print_opt_error(@"Invalid subcommand: $(args[1])", args);
		else {
		    print(help_text);
		    return 0;
		}
	}

	protected int print_error(string message) {
        stderr.printf(@"$(RED + BOLD)ERROR: %s$(RESET)\n", message);
	    return 1;
	}

	protected int print_opt_error(string message, string[] args) {
	    print_error(message);
		print(@"Run $(BOLD)%s --help$(RESET) to see a full list of available command line options.\n",
		    args[0]);
		return 64; // in sysexits.h, 64 indicates a command usage error
	}

	protected int not_enough_arguments_error(string[] args) {
		return print_opt_error("Not enough arguments", args);
	}

    // Entry point for each subcommand
    //
    // Returning -1 tells the parser to execute the appropriate subcommand. If
    // you return -1, args[1] should contain the name of the next subcommand
    // (after options are removed)
	public abstract int main(string[] args) throws Error; // Entry point for each subcommand

    // Allow subcommand to override the enough args check
	protected virtual bool check_enough_args() {
		return false;
	}
}

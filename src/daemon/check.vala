using Updated;
using OSTree.CommitMeta;

async CheckResult check_impl(OSTree.Sysroot sysroot, Progress progress,
        Cancellable cancellable) throws UpdateError {

    Utils.bail_if_cancelled(cancellable);
    var merge = sysroot.get_merge_deployment(OS_NAME);
    var repo = sysroot.repo();
    OTUtils.ensure_writable(repo);

    // Parse origin to get collection refs
    var origin = OTUtils.get_origin(merge);
    OSTree.CollectionRef base_cref, kernel_cref;
    OTUtils.parse_origin(origin, out base_cref, out kernel_cref);

    // Create the output object now with default values
    var output = CheckResult() {
        availability = UpdateAvailability.NONE,
        download_size = -1, // -1 = unknown
        new_version = "none",
        changelog = new HashTable<string, string>(str_hash, str_equal)
    };

    // Find remotes to pull from
    progress.resolving();
    OSTree.RepoFinderResult[] remotes;
    try {
        remotes = yield repo.find_remotes_async({base_cref, kernel_cref},
            null, null, null, cancellable);
    } catch (Error e) {
        Utils.bail_if_cancelled(cancellable);
        throw new UpdateError.NO_REMOTES(e.message);
    }
    progress.finished();

    // Bail if we don't have any remotes available
    if (remotes.length == 0)
        throw new UpdateError.NO_REMOTES("No remotes found");

    Utils.bail_if_cancelled(cancellable);

    // Get the timestamp of the merge commits
    uint64 old_base_timestamp = 0, old_kernel_timestamp = 0;
    if (merge != null)
        try {
            Variant merge_commit, base_commit, kernel_commit;
            repo.load_variant(OSTree.ObjectType.COMMIT, merge.get_csum(),
                out merge_commit);

            var merge_meta = OTUtils.get_commit_metadata(merge_commit);
            var base_csum = merge_meta[KEY_BASE_CSUM] as string;
            var kernel_csum = merge_meta[KEY_KERNEL_CSUM] as string;

            repo.load_variant(OSTree.ObjectType.COMMIT, base_csum,
                out base_commit);
            repo.load_variant(OSTree.ObjectType.COMMIT, kernel_csum,
                out kernel_commit);

            old_base_timestamp = OSTree.commit_get_timestamp(base_commit);
            old_kernel_timestamp = OSTree.commit_get_timestamp(kernel_commit);
        } catch (Error e) {
            critical("Couldn't get timestamps from merge commit: %s", e.message);
        }

    // Check if there's any updates
    uint64 new_base_timestamp = 0, new_kernel_timestamp = 0;
    string? new_base_csum = null, new_kernel_csum = null;
    foreach (var remote in remotes) {
        string? base_csum_candidate = remote.ref_to_checksum[base_cref];
        if (base_csum_candidate != null) {
            var be = remote.ref_to_timestamp[base_cref];
            new_base_timestamp = uint64.from_big_endian(*be);
            new_base_csum = base_csum_candidate;
        }

        string? kernel_csum_candidate = remote.ref_to_checksum[kernel_cref];
        if (kernel_csum_candidate != null) {
            var be = remote.ref_to_timestamp[kernel_cref];
            new_kernel_timestamp = uint64.from_big_endian(*be);
            new_kernel_csum = kernel_csum_candidate;
        }
    }

    if (new_base_csum == null)
        throw new UpdateError.NO_REMOTES("Couldn't resolve base");
    if (new_kernel_csum == null)
        throw new UpdateError.NO_REMOTES("Couldn't resolve kernel");
    if (new_base_timestamp > old_base_timestamp)
        output.availability |= UpdateAvailability.BASE;
    if (new_kernel_timestamp > old_kernel_timestamp)
        output.availability |= UpdateAvailability.KERNEL;

    // Bail early if no updates are available
    if (!output.availability.to_bool()) return output;

    // We have an update; pull down the commit metadata
    progress.pulling();
    try {
        var pull_opts_bld = new VariantDict();
        pull_opts_bld.insert("flags", "i", OSTree.RepoPullFlags.COMMIT_ONLY);
        var pull_opts = pull_opts_bld.end();
        yield repo.pull_from_remotes_async(remotes, pull_opts, null, cancellable);
    } catch (Error e) {
        Utils.bail_if_cancelled(cancellable);
        throw new UpdateError.DOWNLOAD_FAILED(e.message);
    }

    // Compute download size
    // TODO: Teach OSTree to do a dry run w/ repo.pull_from_remotes_async
    var ot_progress = new OSTree.AsyncProgress();
    var pull_crefs = new VariantBuilder(new VariantType("a(sss)"));
    pull_crefs.add("(sss)", base_cref.collection_id, base_cref.ref_name,
        new_base_csum);
    pull_crefs.add("(sss)", kernel_cref.collection_id, kernel_cref.ref_name,
        new_kernel_csum);
    var pull_opts_bld = new VariantDict();
    pull_opts_bld.insert_value("collection-refs", pull_crefs.end());
    pull_opts_bld.insert_value("dry-run", true);
    pull_opts_bld.insert_value("require-static-deltas", true);
    var pull_opts = pull_opts_bld.end();
    foreach (var remote in remotes) {
        try {
            repo.pull_with_options(remote.remote.get_name(), pull_opts,
                ot_progress, cancellable);
            output.download_size = ot_progress.get_uint64("total-delta-part-size");
        } catch (Error e) {
            Utils.bail_if_cancelled(cancellable);
            warning("Couldn't get download size for remote %s: %s",
                remote.remote.get_name(), e.message);
        }
        if (output.download_size != -1) break;
    }
    if (output.download_size == -1)
        warning("Couldn't get download size");
    ot_progress.finish();
    progress.finished();

    // Boost the estimated download size by 1%. This is so the estimate matches
    // the download size reported in Pull. Also, it may be more representative
    // of reality anyways, because OSTree will have to download a little bit
    // of non-delta content. Better to overestimate than underestimate!
    if (output.download_size != -1)
        output.download_size = (uint64)(output.download_size * 1.01);

    // Get metadata from the new base commit
    try {
        Variant new_base_commit;
        repo.load_variant(OSTree.ObjectType.COMMIT, new_base_csum, out new_base_commit);
        var new_base_meta = OTUtils.get_commit_metadata(new_base_commit);

        if (KEY_VERSION in new_base_meta)
            output.new_version = new_base_meta[KEY_VERSION] as string;
        // TODO: Get the changelog
    } catch (Error e) {
        critical("Couldn't get new base's metadata: " + e.message);
    }

    // Return the result
    return output;
}

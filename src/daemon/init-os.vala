using Updated;

void init_os_impl(OSTree.Sysroot target, OSTree.Repo source, KeyFile origin,
        Progress progress, Cancellable cancellable)
        throws UpdateError {
    Utils.bail_if_cancelled(cancellable);

    progress.creating_dirs();

    // Create the filesystem layout
    if (DirUtils.create(target.path.get_child("boot").get_path(), 0755) < 0 &&
        errno != Posix.EEXIST)
        throw new UpdateError.FAILED("Failed to create /boot");
    try {
        target.ensure_initialized(cancellable);
    } catch (Error e) {
        Utils.bail_if_cancelled(cancellable);
        throw new UpdateError.FAILED("Failed to initialize sysroot");
    }
    try {
        target.load(cancellable);
    } catch (Error e) {
        Utils.bail_if_cancelled(cancellable);
        throw new UpdateError.FAILED("Failed to load sysroot");
    }
    var target_repo = target.repo();
    OTUtils.ensure_writable(target_repo);

    // Set repo settings
    var config = target_repo.get_config();
    config.set_string("sysroot", "bootloader", "none");
    config.set_boolean("sysroot", "readonly", true);
    try {
        target_repo.write_config(config);
    } catch (Error e) {
        throw new UpdateError.FAILED("Failed to configure target repo");
    }

    // Create osname
    try {
        target.init_osname(OS_NAME, cancellable);
    } catch (Error e) {
        Utils.bail_if_cancelled(cancellable);
        throw new UpdateError.FAILED("Failed to initialize osname");
    }

    progress.finished();

    Utils.bail_if_cancelled(cancellable);

    // Set up the progress reporting
    var ostree_progress = new OSTree.AsyncProgress();
    uint max_outstanding = 0;
    ostree_progress.changed.connect(() => {
        var outstanding = ostree_progress.get_uint("outstanding-writes");
        if (outstanding > max_outstanding)
            max_outstanding = outstanding;
        if (max_outstanding != 0) {
            var written = max_outstanding - outstanding;
            progress.downloaded_coarse(written, max_outstanding);
        }
    });
    progress.pulling();

    // Parse the origin
    OSTree.CollectionRef base_cref, kernel_cref;
    OTUtils.parse_origin(origin, out base_cref, out kernel_cref);
    string[] refs = { base_cref.ref_name, kernel_cref.ref_name };
    string[] commits = { "", "" };
    try {
        for (int i = 0; i < refs.length; i++)
            source.resolve_rev(refs[i], true, out commits[i]);
    } catch {
        Utils.bail_if_cancelled(cancellable);
        throw new UpdateError.FAILED("Failed to resolve collection refs");
    }

    // Set up the pull options
    var options = new HashTable<string, Variant>(str_hash, str_equal);
    options["refs"] = refs;
    options["override-commit-ids"] = commits;

    // Do the pull
    try {
        target_repo.pull_with_options(source.path.get_uri(), options,
            ostree_progress, cancellable);
    } catch (Error e) {
        Utils.bail_if_cancelled(cancellable);
        throw new UpdateError.DOWNLOAD_FAILED(e.message);
    }

    // Notify that we're done
    ostree_progress.finish();
    progress.finished();
}

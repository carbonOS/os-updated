using Cli;

class RollbackCommand : SubCommand {
	// Defines option parsing for the subcommand
	private static bool opt_pin;
	private const OptionEntry[] _options = {
		{ "pin", 'p', 0, OptionArg.NONE, ref opt_pin, "Pin the deployment the system is rolling back to", null },
		{ null }
	};

	// Configure the subcommand
	construct {
		name = "rollback";
		aliases = { "undo", "undeploy" };
		desc = "Undo system updates";
		options = _options;
		error_prefix = "Failed to rollback updates";
	}

	// Entry point into the subcommand
	public override int main(string[] args) throws Error {
		var daemon = Daemon.get_main_service();
		var progress = new Daemon.TaskOutput(daemon);
		progress.resolving_msg = "Resolving deployments";
		progress.deploying_msg = "Removing deployment";

		var needs_reboot = false, done = false;
		daemon.rollback.begin(opt_sysroot, opt_pin, (obj, res) => {
			try {
				needs_reboot = daemon.rollback.end(res);
				done = true;
			} catch (Error e) {
				progress.failed(e);
			}
		});
		while (!done) progress.iteration();

		if (needs_reboot) {
			print("Done! Reboot the system to apply changes.\n");
			print(@"Run $(BOLD)systemctl reboot$(UNBOLD) to start a reboot\n");
		}
		return 0;
	}
}

using Updated;

namespace OTUtil {

    const string NAME_PATTERN ="""^(?:(?'idx'\d+)|(?'csum'[[:xdigit:]]+)\.(?'bootserial'\d+)|rollback|booted|pending|current)$""";
    public int parse_deployment_name(Deployment[] deployments, string name)
            throws Error {
        // Match the regex pattern
        Regex regex = new Regex(NAME_PATTERN);
        MatchInfo match_info;
        var matches = regex.match(name, 0, out match_info);
        if (!matches) throw new UpdateError.FAILED("Invalid deployment name");

        // Handle idx
        var idx_s = match_info.fetch_named("idx");
        if (idx_s != "") {
            var idx = int.parse(idx_s);
            if (idx >= deployments.length)
                throw new UpdateError.FAILED("No matching deployment found");
            return idx;
        }

        // Handle csum.bootserial
        var csum = match_info.fetch_named("csum");
        var bootserial = int.parse(match_info.fetch_named("bootserial"));
        if (csum != "") {
            int[] matching = {};
            for (var idx = 0; idx < deployments.length; idx++) {
                var it = deployments[idx];
                if (it.csum.has_prefix(csum) && it.deploy_serial == bootserial)
                    matching += idx;
            }

            if (matching.length > 1)
                throw new UpdateError.FAILED("Ambiguous checksum");
            if (matching.length == 0)
                throw new UpdateError.FAILED("No matching deployment found");
            return matching[0];
        }

        // Find interesting deployments
        Deployment? rollback = null, booted = null, pending = null, curr = null;
        foreach (var it in deployments) {
            if (it.is_rollback) rollback = it;
            if (it.is_booted) booted = it;
            if (it.is_pending) pending = it;
        }
        curr = (booted != null) ? booted : pending;

        // Handle named deployments
        Deployment target;
        switch (name) {
            case "rollback":
                if (rollback != null)
                    target = rollback;
                else
                    throw new UpdateError.FAILED("No rollback deployment found");
                break;
            case "booted":
                if (booted != null)
                    target = booted;
                else
                    throw new UpdateError.FAILED("No booted deployment found");
                break;
            case "pending":
                if (pending != null)
                    target = pending;
                else
                    throw new UpdateError.FAILED("No pending deployment found");
                break;
            case "current":
                if (curr != null)
                    target = curr;
                else
                    throw new UpdateError.FAILED("No current deployment found");
                break;
            default:
                assert_not_reached();
        }

        // Return the index
        for (var idx = 0; idx < deployments.length; idx++)
            if (target == deployments[idx])
                return idx;
        assert_not_reached();
    }
}

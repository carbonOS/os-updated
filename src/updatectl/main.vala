public int main(string[] args) {
    Intl.setlocale(); // Make sure we have the locale setup

    var newargs = args.copy();
    newargs[0] = Path.get_basename(args[0]);
	return new MainCommand().invoke(newargs);
}

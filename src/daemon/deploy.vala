using Updated;
using OSTree.CommitMeta;

private string read_os_version(File root, Cancellable cancellable) throws Error {
    // Read /usr/lib/os-release out of the root
    var file = root.resolve_relative_path("usr/lib/os-release");
    var istream = file.read(cancellable);
    var data = new DataInputStream(istream);

    // Look for the VERSION_ID
    string? line;
    while ((line = data.read_line(null, cancellable)) != null)
        if (line.has_prefix("VERSION_ID="))
            return Shell.unquote(line.substring(11));
    throw new IOError.NOT_FOUND("Couldn't read OS version");
}

async void deploy_impl(OSTree.Sysroot sysroot, bool no_merge,
        KeyFile? origin_override, Progress progress, Cancellable cancellable)
        throws UpdateError {
    Utils.bail_if_cancelled(cancellable);
    var repo = sysroot.repo();
    OTUtils.ensure_writable(repo);
    var merge = sysroot.get_merge_deployment(OS_NAME);
    var origin = origin_override ?? OTUtils.get_origin(merge);

    // Start the repository transaction
    progress.merging();
    try {
        repo.prepare_transaction(null, cancellable);
    } catch (Error e) {
        throw new UpdateError.MERGE_FAILED("Couldn't start transaction: " +
            e.message);
    }
    string commit_csum; // We'll need it outside the try/catch
    try {
        // Construct the refs we'll be combining
        OSTree.CollectionRef base_cref, kernel_cref;
        var arch = OTUtils.parse_origin(origin, out base_cref, out kernel_cref);

        // Resolve the crefs
        string? base_csum = null, kernel_csum = null;
        var resolve_flags = OSTree.RepoResolveRevExtFlags.NONE;
        repo.resolve_collection_ref(base_cref, true, resolve_flags,
            out base_csum, cancellable);
        repo.resolve_collection_ref(kernel_cref, true, resolve_flags,
            out kernel_csum, cancellable);

        // If we couldn't resolve the crefs, fall back to the local repo
        if (base_csum == null)
            repo.resolve_rev(base_cref.ref_name, false, out base_csum);
        if (kernel_csum == null)
            repo.resolve_rev(kernel_cref.ref_name, false, out kernel_csum);

        // Get content from the repo
        GLib.File base_root_gfile, kernel_root;
        repo.read_commit(base_csum, out base_root_gfile, null, cancellable);
        repo.read_commit(kernel_csum, out kernel_root, null, cancellable);
        OSTree.RepoFile base_root = base_root_gfile as OSTree.RepoFile;

        // Generate the MutableTree
        // TODO: Port to `new MutableTree.from_commit(...)`
        var mtree = new OSTree.MutableTree.from_checksum(repo,
            base_root.tree_get_contents_checksum(),
            base_root.tree_get_metadata_checksum());
        repo.write_directory_to_mtree(kernel_root, mtree, null, cancellable);

        // TODO: Extension seed would go here:
        // Call MutableTree.ensure_parent_dirs to create place for ext-seed
        //   For systemd-sysext to pick it up, needs to go into /usr/lib/extensions
        // Call Repo.write_symlink to create the symlink in the repository & get checksum
        // Call MutableTree.replace_file to insert ext-seed into the tree

        // Write the MutableTree
        File commit_root;
        repo.write_mtree(mtree, out commit_root, cancellable);

        // Check if the content changed
        string? parent_commit = null;
        var skip_commit = false;
        if (!no_merge && merge != null) {
            parent_commit = merge.get_csum();
            File parent_root;
            repo.read_commit(parent_commit, out parent_root, null, cancellable);
            skip_commit = parent_root.equal(commit_root);
        }

        if (!skip_commit) {
            // Generate commit metadata
            VariantDict commit_meta = new VariantDict();
            OSTree.CommitMeta.add_for_bootable(commit_root, commit_meta,
                cancellable);
            var os_version = read_os_version(commit_root, cancellable);
            commit_meta.insert_value(KEY_VERSION, os_version);
            commit_meta.insert_value(KEY_BASE_CSUM, base_csum);
            commit_meta.insert_value(KEY_KERNEL_CSUM, kernel_csum);

            // Write the commit
            repo.write_commit(parent_commit, null, null, commit_meta.end(),
                commit_root as OSTree.RepoFile, out commit_csum, cancellable);

            // Finish the transaction
            var commit_refspec = "os/%s".printf(arch);
            repo.transaction_set_ref(null, commit_refspec, commit_csum);
            repo.commit_transaction(null, cancellable);
        } else {
            // Content hasn't changed since parent so just reuse that
            commit_csum = parent_commit;
            repo.abort_transaction(cancellable);
        }
    } catch (Error e) {
        // If anything goes wrong, abort the transaction
        try {
            repo.abort_transaction(cancellable);
        } catch (Error abt_e) {
            critical("Abort transaction failed: %s", abt_e.message);
        }

        Utils.bail_if_cancelled(cancellable);
        throw new UpdateError.MERGE_FAILED(e.message);
    }
    progress.finished();

    // Clean up any failed past deployment attempts
    try {
        sysroot.prepare_cleanup(cancellable);
    } catch (Error e) {
        Utils.bail_if_cancelled(cancellable);
        throw new UpdateError.FAILED("Couldn't clean up: " + e.message);
    }

    Utils.bail_if_cancelled(cancellable);

    // TODO: kargs, overlay initrds
    // TODO: Fix OSTree.KernelArgs object in vapi
    var deploy_opts = OSTree.SysrootDeployTreeOpts();
    deploy_opts.override_kernel_argv = { // TODO: Better way to set default kargs
        "root=PARTLABEL=carbon-root",
        "rootflags=compress=zstd:1",
        "quiet",
        "splash",
        "rw"
    };
    deploy_opts.overlay_initrds = null;

    progress.deploying();
    OSTree.Deployment? new_deployment = null;
    try {
        if (!no_merge && sysroot.is_booted()) { // Check if we can stage
            sysroot.stage_tree_with_options(OS_NAME, commit_csum, origin, merge,
                deploy_opts, out new_deployment, cancellable);
        } else {
            sysroot.deploy_tree_with_options(OS_NAME, commit_csum, origin,
                no_merge ? null : merge, deploy_opts, out new_deployment,
                cancellable);

            // Finalize the deployment
            if (no_merge) {
                var deployments = new GenericArray<weak OSTree.Deployment>(1);
                deployments.add(new_deployment);
                var write_opts = OSTree.SysrootWriteDeploymentsOpts() {
                    do_postclean = false
                };
                sysroot.write_deployments_with_options(deployments, write_opts,
                    cancellable);
            } else {
                sysroot.simple_write_deployment(OS_NAME, new_deployment, merge,
                    OSTree.SysrootSimpleWriteDeploymentFlags.NO_CLEAN,
                    cancellable);
            }
        }
    } catch (Error e) {
        Utils.bail_if_cancelled(cancellable);
        throw new UpdateError.DEPLOY_FAILED(e.message);
    }
    if (new_deployment == null)
        throw new UpdateError.DEPLOY_FAILED("Missing new deployment");
    progress.finished();

    Utils.bail_if_cancelled(cancellable);

    // Cleanup
    progress.pruning();
    try {
        sysroot.cleanup(cancellable);
    } catch (Error e) {
        Utils.bail_if_cancelled(cancellable);
        throw new UpdateError.FAILED("Failed to prune repo");
    }
    progress.finished();
}

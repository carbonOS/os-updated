using Updated;
using Cli;

class RebootCommand : SubCommand {
	// Configure the subcommand
	construct {
		name = "reboot";
		desc = "Reboot into a specific OS deployment";
		arguments = "DEPLOYMENT";
		expected_args = 1;
		error_prefix = "Failed to reboot";
	}

	[DBus(name="org.freedesktop.login1.Manager")]
	private interface Systemd : DBusProxy {
        public abstract void set_reboot_to_boot_loader_entry(string e) throws Error;
        public abstract void reboot(bool interactive) throws Error;
	}

	// Entry point into the subcommand
	public override int main(string[] args) throws Error {
	    // Get the deployments from the daemon
	    var daemon = Daemon.get_main_service();
	    bool is_booted;
        var deployments = daemon.get_deployments(opt_sysroot, out is_booted);

        // Bail if the sysroot isn't booted (i.e. the bootloader wouldn't boot
        // into it either)
        if (!is_booted)
            throw new UpdateError.FAILED("This sysroot is not booted");

        // Bail if we have a staged deployment, since we cannot reliably predict
        // what the bootloader layout will be like after finalize-staged runs
        // (is the deployment we're rebooting into gone? did it fail? etc)
        foreach (var it in deployments)
            if (it.is_staged)
                throw new UpdateError.FAILED("Unsupported with staged deployments");

        // Parse out the deployment we're interested in
        var idx = OTUtil.parse_deployment_name(deployments, args[1]);
        var deployment = deployments[idx];

        // Error out if we're already booted into the deployment
        if (deployment.is_booted)
            throw new UpdateError.FAILED("Already booted into this deployment");

        // Construct the correct boot entry name
        var entry_idx = (deployments.length - idx);
        var entry = "ostree-%d-%s.conf".printf(entry_idx, deployment.os_name);

        // Ask systemd to reboot
        var proxy = Bus.get_proxy_sync<Systemd>(BusType.SYSTEM,
            "org.freedesktop.login1", "/org/freedesktop/login1");
        proxy.set_reboot_to_boot_loader_entry(entry);
        proxy.reboot(true);

		return 0;
	}
}

using Updated;
using OSTree.CommitMeta;

inline Deployment[] get_deployments_impl(OSTree.Sysroot sysroot,
        out bool is_booted) throws UpdateError {
    var repo = sysroot.repo();

    is_booted = sysroot.is_booted();
    Deployment[] out_deployments = {};

    // Get special deployments
    var booted = sysroot.get_booted_deployment();
    OSTree.Deployment? pending, rollback;
    sysroot.query_deployments_for(OS_NAME, out pending, out rollback);
    sysroot.get_deployments().foreach(it => {
        var dep = Deployment();

        // Get basic info
        dep.os_name = it.get_osname();
        dep.csum = it.get_csum();
        dep.deploy_serial = it.get_deployserial();
        dep.unlock_state = it.get_unlocked();
        dep.is_booted = it.equal(booted);
        dep.is_staged = it.is_staged();
        dep.is_pending = it.equal(pending);
        dep.is_rollback = it.equal(rollback);
        dep.is_pinned = it.is_pinned();
        dep.is_pending_delete = OTUtils.check_pending_deletion(it);

        // Get commit metadata
        dep.metadata = new HashTable<string, string>(str_hash, str_equal);
        try {
            Variant commit;
            repo.load_variant(OSTree.ObjectType.COMMIT, it.get_csum(),
                out commit);

            dep.timestamp = OSTree.commit_get_timestamp(commit);

            var meta = OTUtils.get_commit_metadata(commit);
            if (KEY_VERSION in meta)
                dep.metadata["version"] = meta[KEY_VERSION] as string;
            if (KEY_LINUX in meta)
                dep.metadata["linux"] = meta[KEY_LINUX] as string;
            if (KEY_BASE_CSUM in meta)
                dep.metadata["base_csum"] = meta[KEY_BASE_CSUM] as string;
            if (KEY_KERNEL_CSUM in meta)
                dep.metadata["kernel_csum"] = meta[KEY_KERNEL_CSUM] as string;
        } catch (Error e) {
            warning("Couldn't get deployment metadata: %s", e.message);
        }

        // Get remote info
        var origin = it.get_origin();
        if (origin != null) {
            try {
                dep.metadata["collection"] = origin.get_string("origin", "Collection");
            } catch {}
            try {
                dep.metadata["base_variant"] = origin.get_string("origin", "BaseVariant");
            } catch {}
            try {
                dep.metadata["kernel_variant"] = origin.get_string("origin", "KernelVariant");
            } catch {}
        }

        out_deployments += dep;
    });

    return out_deployments;
}

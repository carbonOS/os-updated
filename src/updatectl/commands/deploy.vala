using Cli;

class DeployCommand : SubCommand {
	// Defines option parsing for the subcommand
	private static bool no_download;
	private const OptionEntry[] _options = {
		{ "no-download", 'n', 0, OptionArg.NONE, ref no_download, "Do not attempt to download an update", null },
		{ null }
	};

	// Configure the subcommand
	construct {
		name = "deploy";
		aliases = { "u", "update", "d" };
		desc = "Deploy system updates";
		options = _options;
		error_prefix = "Failed to deploy updates";
	}

	// Entry point into the subcommand
	public override int main(string[] args) throws Error {
        var daemon = Daemon.get_main_service();
        var progress = new Daemon.TaskOutput(daemon);
        bool done = false;

        if (!no_download) { // Ask the daemon to pull
	        daemon.pull.begin(opt_sysroot, (obj, res) => {
	            try {
	                daemon.pull.end(res);
	                done = true;
	            } catch (Error e) {
	                progress.failed(e);
	            }
	        });
	        while (!done) progress.iteration();
        }

        // Ask the daemon to deploy
        done = false;
        daemon.deploy.begin(opt_sysroot, (obj, res) => {
            try {
                daemon.deploy.end(res);
                done = true;
            } catch (Error e) {
                progress.failed(e);
            }
        });
        while (!done) progress.iteration();

        // If --sysroot was passed, assume we're not dealing with a booted system
        if (opt_sysroot == "") {
            print("Done! Reboot the system to apply changes.\n");
            print(@"Run $(BOLD)systemctl reboot$(UNBOLD) to start a reboot\n");
        }
		return 0;
	}
}

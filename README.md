# os-updated

This is carbonOS's update manager. Its a simple privalaged helper daemon that
allows clients like `updatectl` (CLI) and GNOME Software (GUI) to upgrade the
system. It also provides factory reset functionality and manages kernel arguments.
This is all built on top of [OSTree](https://ostreedev.github.io)

## Updates

carbonOS is split into two major components: the base OS and the kernel image.
The base OS is everything except for the kernel, initramfs, and other device-specific
drivers. The kernel image "primes" a base image into a bootable image. `os-updated`
keeps the system up to date by simply pulling down the latest base and kernel images,
combining them into a single image via OSTree's API, and then deploying that image.

## Building

Development is easiest on carbonOS through GNOME Builder. Simply switch carbonOS
to the devel variant:

```bash
$ updatectl switch --base=devel
[Enter your credentials at the prompt]
[Reboot]
```

and then hit run in Builder. This will build and install the project, so
Polkit will prompt you for permission. Then Builder will run `test/run`. This
will generate a fake OS sysroot, then drop you into a testing shell where
`updatectl` is automatically configured to use the fake OS sysroot. You can
verify this with `updatectl status`, which should report that it's running
`carbonOS 1970.1`. Running `test/run` will prompt for an administrator
password, because `os-updated` needs to run as root, and the script needs
root privilages at times to clean up after itself and to initialize the OS

In the test shell, you can run `commit-os-update` with a list of targets (see
supported targets below) to generate & "publish" new versions of the fake
OS, which `os-updated` should detect and treat as valid. If `os-updated`
complains that there's no remotes, you can run `srv restart` to restart the
local ostree repo server.

Here's the list of targets supported by `commit-os-update`:
- `base` or `desktop`: A `desktop` OS base image, 10Mb in size
- `base-big` or `desktop-big`: A `desktop` OS base image, 300Mb in size
- `kernel`: A `mainline` kernel image, 10Mb in size
- `kernel-big`: A `mainline` kernel image, 100Mb in size
- `devel`: A `devel` OS base image, 10Mb in size
- `devel-big`: A `devel` OS base image, 300Mb in size

Once you're done with `os-updated` development, you can stop the running web
server with `test/srv stop`


namespace Updated {
    [DBus(name="sh.carbon.update1.Error")]
    [CCode(cname="UpdatedError", type_id="UPDATED_ERROR", cprefix="UPDATED_ERROR_")]
    public errordomain UpdateError {
        CANCELLED,
        NO_REMOTES,
        ACCESS_DENIED,
        DOWNLOAD_FAILED,
        MERGE_FAILED,
        DEPLOY_FAILED,
        NO_SPACE, // TODO: OSTree can't tell us this specifically
        LIVE_OS,
        NO_LOCK,
        FAILED
    }

    // A deployment returned by GetDeployments
    public struct Deployment {
        string csum;
        int deploy_serial;
        string os_name;

        bool is_booted;
        bool is_staged;
        bool is_pending;
        bool is_rollback;
        bool is_pinned;
        bool is_pending_delete;
        OSTree.DeploymentUnlockedState unlock_state;

        HashTable<string, string> metadata;
        uint64 timestamp;
    }

    // Whether or not an update is available
    [Flags]
    public enum UpdateAvailability {
        BASE, // The base has an update available
        KERNEL, // The kernel has an update available
        NONE = 0; // Everything is up-to-date

        public bool to_bool() {
            return this != NONE;
        }
    }

    // The results of a pull (used by check)
    public struct CheckResult {
        UpdateAvailability availability; // What is available for updates
        uint64 download_size; // Download size of the new update; -1 for unknown
        string new_version; // Version number of the new update; "none" for unknown
        HashTable<string, string> changelog; // maps language -> changelog
    }
}

using Updated;

async bool rollback_impl(OSTree.Sysroot sysroot, bool pin, Progress progress,
        Cancellable cancellable) throws UpdateError {
    Utils.bail_if_cancelled(cancellable);

    // Find the victim deployment
    progress.resolving();
    unowned OSTree.Deployment victim;
    var deployments = sysroot.get_deployments();

    OSTree.Deployment? booted, staged, pending;
    booted = sysroot.get_booted_deployment();
    staged = sysroot.get_staged_deployment();
    sysroot.query_deployments_for(OS_NAME, out pending, null);

    var count = 0; // Count our deployments
    foreach (var elem in deployments.data) {
        if (elem.get_osname() == OS_NAME &&
            !OTUtils.check_pending_deletion(elem))
            count++;
        if (count >= 2) break;
    }
    if (count < 2)
        throw new UpdateError.FAILED("Nothing to roll back to");

    if (staged != null) {
        // If we have a staged deployment, always undeploy it
        victim = staged;
    } else if (pending != null) {
        if (pending.is_pinned())
            throw new UpdateError.FAILED("Pending deployment is pinned");
        victim = pending;
    } else if (booted != null) {
        if (booted.is_pinned())
            throw new UpdateError.FAILED("Current deployment is pinned");
        victim = booted;
    } else { // pending == null implies booted != null & vice versa
        critical("rollback: pending=null AND booted=null!");
        throw new UpdateError.FAILED("Found no deployments to remove");
    }
    progress.finished();

    Utils.bail_if_cancelled(cancellable);

    // Remove the deployment
    progress.deploying();
    if (victim != booted) {
        deployments.remove(victim);
    } else {
        // The booted deployment needs special treatment since we can't just
        // delete it (the system is running from it!).

        // Move the booted deployment to the end of the list
        deployments.remove(victim);
        deployments.insert(-1, victim);

        // Mark the deployment for deletion
        var origin = victim.clone().get_origin();
        origin.set_boolean("libostree-transient", "x-pending-delete", true);
        try {
            sysroot.write_origin_file(victim, origin, cancellable);
        } catch {
            Utils.bail_if_cancelled(cancellable);
            throw new UpdateError.FAILED(
                "Failed to mark booted deployment for deletion");
        }
    }
    var write_opts = OSTree.SysrootWriteDeploymentsOpts() {
        do_postclean = false
    };
    try {
        sysroot.write_deployments_with_options(deployments, write_opts,
            cancellable);
    } catch (Error e) {
        Utils.bail_if_cancelled(cancellable);
        throw new UpdateError.DEPLOY_FAILED(e.message);
    }
    progress.finished();

    Utils.bail_if_cancelled(cancellable);

    // Cleanup
    progress.pruning();
    try {
        sysroot.cleanup(cancellable);
    } catch (Error e) {
        Utils.bail_if_cancelled(cancellable);
        throw new UpdateError.FAILED("Failed to prune repo");
    }
    progress.finished();

    // Pin the rollback deployment if requested
    if (pin) {
        // Find the deployment we rolled back to
        unowned OSTree.Deployment? to_pin = null;
        foreach (var elem in deployments.data)
            if (elem.get_osname() == OS_NAME) {
                to_pin = elem;
                break;
            }

        // Pin it
        if (to_pin != null && !to_pin.is_pinned()) {
            try {
                sysroot.deployment_set_pinned(to_pin, true);
            } catch {
                throw new UpdateError.FAILED("Failed to pin rollback deployment");
            }
        }
    }

    return (victim == booted);
}


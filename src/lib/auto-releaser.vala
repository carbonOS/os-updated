// Automatically calls release on the provided service when it goes out of scope.
// Use this with a g_autoptr in C
public class Updated.AutoReleaser : Object {
    private Updated.Service service;

    public AutoReleaser(Updated.Service service) {
        this.service = service;
    }

    ~AutoReleaser() {
        try {
            this.service.release();
        } catch (Error e) {
            // Not much we can do...
            critical("Failed to release service: %s", e.message);
        }
    }
}

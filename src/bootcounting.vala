// This program enables bootcounting on the latest deployment after
// ostree-finalize-staged finishes.

using Linux;

const string ENTRYDIR = "/boot/loader/entries";

void main() {
    // Remount boot rw in a mount namespace
    if (unshare(Linux.CloneFlags.NEWNS) != 0)
        error("Couldn't set up mount namespace: %m");
    if (mount("/boot", "/boot", "", MountFlags.REMOUNT | MountFlags.RELATIME) != 0)
        error("Couldn't remount /boot rw: %m");

    // Enumerate the entries and select the right one to enable boot counting
    string? entry = null, filename = null;
    Dir dir = null;
    try {
        dir = Dir.open(ENTRYDIR);
    } catch {
        error(@"Couldn't open $ENTRYDIR");
    }
    while ((filename = dir.read_name()) != null) {
        if (!filename.has_prefix("ostree-") || !filename.has_suffix(".conf"))
            continue;
        if ("+" in filename)
            print("Boot counting already enabled on entry %s\n", filename);
        if (entry < filename)
            entry = filename;
    }
    if (entry == null)
        error("No entries found");
    if ("+" in entry)
        print("Resetting boot counting for %s\n", entry);
    else
        print("Enabling boot counting for %s\n", entry);

    // Create the new name for the entry
    var end_index = entry.index_of(".conf");
    var start_index = ("+" in entry) ? entry.index_of_char('+') : end_index;
    var new_name = entry.splice(start_index, end_index, "+3");
    print("Renaming %s -> %s\n", entry, new_name);
    FileUtils.rename(@"$ENTRYDIR/$entry", @"$ENTRYDIR/$new_name");
}

#pragma once

#include <glib.h>
#include <glib-object.h>
#include <gnome-software.h>

G_BEGIN_DECLS

#define GS_TYPE_PLUGIN_OS_UPDATED (gs_plugin_os_updated_get_type())

G_DECLARE_FINAL_TYPE (GsPluginOsUpdated, gs_plugin_os_updated, GS, PLUGIN_OS_UPDATED, GsPlugin)

G_END_DECLS


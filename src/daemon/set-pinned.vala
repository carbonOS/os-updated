using Updated;

async void set_pinned_impl(OSTree.Sysroot sysroot, int idx, bool pinned,
        Cancellable cancellable) throws Error {
    Utils.bail_if_cancelled(cancellable);

    var deployments = sysroot.get_deployments();
    if (idx < 0 || idx >= deployments.length)
        throw new DBusError.INVALID_ARGS("Invalid deployment index");
    var deployment = deployments[idx];

    try {
        sysroot.deployment_set_pinned(deployment, pinned);
    } catch (Error e) {
        throw new UpdateError.FAILED(e.message);
    }
}


[DBus(name="sh.carbon.update1")]
public interface Updated.Service : DBusProxy {
    [CCode(cname="updated_service_new")]
    public static Service create(Cancellable? c = null) throws Error {
        Service proxy = Bus.get_proxy_sync(BusType.SYSTEM, "sh.carbon.update1",
            "/sh/carbon/update1", DBusProxyFlags.NONE, c);
        if (proxy.g_name_owner == null)
            throw new DBusError.NAME_HAS_NO_OWNER("Daemon not running");
        proxy.set_default_timeout(int.MAX); // Never time out
        return proxy;
    }

    [CCode(cname="updated_service_new_async")]
    public static async Service create_async(Cancellable? c = null) throws Error {
        Service proxy = yield Bus.get_proxy(BusType.SYSTEM, "sh.carbon.update1",
            "/sh/carbon/update1", DBusProxyFlags.NONE, c);
        if (proxy.g_name_owner == null)
            throw new DBusError.NAME_HAS_NO_OWNER("Daemon not running");
        proxy.set_default_timeout(int.MAX); // Never time out
        return proxy;
    }

    [DBus(visible = false)]
    public Progress get_progress(Cancellable? c = null) throws Error {
        var path = this.get_progress_path();
        return Bus.get_proxy_sync(BusType.SYSTEM, "sh.carbon.update1", path,
            DBusProxyFlags.NONE, c);
    }

    /////////////////
    // Daemon APIs //
    /////////////////

    // The version of os-updated
    public abstract string version { owned get; }

    // The OS os-updated is configured for
    public abstract string os_name { owned get; }

    // Used by a client to tell the daemon that it is safe to exit
    // Called automatically when the client disconnects from the bus
    public abstract void release() throws Error;

    // Used by a client to cancel the current operation
    // Called automatically if the client asks to start another operation or
    // disconnects from the bus
    public abstract void cancel() throws Error;

    // Used by a client to get a Progress object that it can monitor
    [DBus(name="GetProgress")]
    public abstract ObjectPath get_progress_path() throws Error;

    ///////////////
    // Main APIs //
    ///////////////

    // Returns the list of OSTree deployments. Used by `updatectl status`
    public abstract Deployment[] get_deployments(string sysroot,
        out bool is_booted) throws Error;

    // Queries remotes to check for updates
    public abstract async CheckResult check(string sysroot) throws Error;

    // Queries remotes to download the latest repository contents
    public abstract async void pull(string sysroot) throws Error;

    // Applies the latest version of the OS from the repo:
    // 1. Combines the base and kernel into a single os/ARCH ref
    // 2. Generates kernel args & overlay initrds
    // 3. Stages a deployment
    public abstract async void deploy(string sysroot) throws Error;

    // Appropriately un-deploys the latest version of the OS
    // pin: Whether or not to pin the deployment we're rolling back to
    // Returns: true if a reboot is necessary, false otherwise
    public abstract async bool rollback(string sysroot, bool pin) throws Error;

    // Factory resets the machine
    public abstract async void factory_reset(string sysroot) throws Error;

    // Changes where/how os-updated looks for updates:
    // - base_variant sets the variant of the base OS image
    // - kernel_variant sets the variant of the kernel image
    // - colection sets the collection to look for
    // A special value of "<keep>" means don't change that field
    public abstract async void switch_origin(string sysroot,
        string base_variant, string kernel_variant, string collection)
        throws Error;

    // Set whether or not the given deployment (index) is pinned
    public abstract async void set_pinned(string sysroot, int deployment,
        bool pinned) throws Error;

    ///////////////
    // Installer //
    ///////////////

    // Initializes & populates an OSTree system
    // 1. Creates basic file structure in target
    // 2. Configures the repo (readonly sysroot, bootloader type, etc)
    // 3. Initializes the "carbonOS" osname
    // 4. Pulls repo contents from source into target
    public abstract async void init_os(string target, string source)
        throws Error;

    /////////////////////////////////////////////////////////////////
    // Blocking Alternatives                                       //
    // These iterate the main loop, so you can use Progress        //
    /////////////////////////////////////////////////////////////////

    [DBus(visible = false)]
    public CheckResult check_sync(string sysroot) throws Error {
        var ctx = MainContext.get_thread_default();

        CheckResult? result = null;
        Error? err = null;
        this.check.begin(sysroot, (obj, res) => {
            try {
                result = this.check.end(res);
            } catch (Error e) {
                err = e;
            }
        });
        while (result == null && err == null)
            ctx.iteration(true); // Block until there's something to do

        if (err != null) throw err;
        return result;
    }

    [DBus(visible = false)]
    public void pull_sync(string sysroot) throws Error {
        var ctx = MainContext.get_thread_default();

        bool done = false;
        Error? err = null;
        this.pull.begin(sysroot, (obj, res) => {
            try {
                this.pull.end(res);
                done = true;
            } catch (Error e) {
                err = e;
            }
        });
        while (!done && err == null)
            ctx.iteration(true); // Block until there's something to do

        if (err != null) throw err;
    }

    [DBus(visible = false)]
    public void deploy_sync(string sysroot) throws Error {
        var ctx = MainContext.get_thread_default();

        bool done = false;
        Error? err = null;
        this.deploy.begin(sysroot, (obj, res) => {
            try {
                this.deploy.end(res);
                done = true;
            } catch (Error e) {
                err = e;
            }
        });
        while (!done && err == null)
            ctx.iteration(true); // Block until there's something to do

        if (err != null) throw err;
    }
}

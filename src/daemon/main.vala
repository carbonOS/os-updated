using Updated;
namespace Daemon {
    [Compact]
    private class ClientData {
        public uint dbus_watch;
        public Cancellable? cancellable;
        public ProgressImpl progress;
    }

    private HashTable<string, ClientData> clients;
    private MainLoop main_loop;
    private uint quit_timeout = 0;
    private bool disable_timeout = false;
    private bool is_liveos;

    int main(string[] args) {
        disable_timeout = ("--no-timeout" in args);

        // Set up the mount namespace
        if (Linux.unshare(Linux.CloneFlags.NEWNS) != 0)
            error("Couldn't set up mount namespace");

        // Check if we're running in a live environment
        string cmdline;
        try {
            FileUtils.get_contents("/proc/cmdline", out cmdline);
            is_liveos = Regex.match_simple("\\bcarbon\\.liveos\\b", cmdline);
        } catch (Error e) {
            warning("Couldn't read /proc/cmdline: %s", e.message);
            is_liveos = false; // Assume we're installed
        }

        // Setup internal state
        main_loop = new MainLoop();
        clients = new HashTable<string, ClientData>(str_hash, str_equal);

        // Connect to dbus
        Bus.own_name(BusType.SYSTEM, "sh.carbon.update1", BusNameOwnerFlags.NONE,
            (conn, name) => { // Bus acquired
                conn.add_filter(dbus_filter);

                try {
                    conn.register_object<MainService>("/sh/carbon/update1",
                        new MainService());
                } catch (Error e) {
                    error("Couldn't register object: %s", e.message);
                }
                message("Ready");
            }, null, (conn, name) => { // Name lost
                error("Lost DBus name");
            });

        // Process messages
        main_loop.run();
        return 0;
    }

    // Watches for new incoming clients and registers them if necessary
    private DBusMessage dbus_filter(DBusConnection conn, owned DBusMessage message,
            bool incoming) {
        if (incoming) {
            var iface = message.get_interface();
            if (iface == null || !iface.has_prefix("sh.carbon"))
                return message; // Pass the message through

            var sender = message.get_sender();
            if (sender == null) {
                warning("Picked up a DBusMessage w/o a sender!");
                return message; // Pass the message through
            }

            if (!clients.contains(sender)) {
                // Log it
                GLib.message("New client: %s", sender);

                // Watch the client and allocate a cancellable
                clients[sender] = new ClientData() {
                    dbus_watch = Bus.watch_name(BusType.SYSTEM, sender,
                        BusNameWatcherFlags.NONE, null, unregister_client),
                    cancellable = null, // nothing to cancel yet
                    progress = new ProgressImpl(conn, sender)
                };

                // Cancel an queued quit
                if (quit_timeout != 0) {
                    Source.remove(quit_timeout);
                    quit_timeout = 0;
                }
            }
        }
        return message; // Pass the message right through
    }

    // Unregisters any registered clients
    void unregister_client(DBusConnection? conn, string client) {
        if (!clients.contains(client)) { // Sanity check
            warning("Unregistered client: %s!", client);
            return;
        }

        // Log it
        var what_happened = (conn == null) ? "Released by" : "Lost";
        message("%s client: %s", what_happened, client);

        // Clean up after the client
        Bus.unwatch_name(clients[client].dbus_watch);
        clients[client].cancellable.cancel(); // Terminate ungoing work
        clients[client].progress.deregister(); // Unregister the progress
        clients.remove(client);

        // Queue up a quit if we have no more clients
        if (clients.length == 0 && !disable_timeout)
            quit_timeout = Timeout.add(10000 /* 10s */, () => {
                message("Timed out with no clients. Quitting...");
                main_loop.quit();
                return Source.REMOVE;
            });
    }

    // Get a cancellable for this client that is linked to the Cancel method
    Cancellable get_cancellable(string client) {
        if (!clients.contains(client)) {
            warning("Unregistered client: %s!", client);
            return new Cancellable(); // Return a dummy cancellable
        }

        unowned var obj = clients[client]; // Perform lookup only once
        if (obj.cancellable != null) obj.cancellable.cancel();
        obj.cancellable = new Cancellable();
        return obj.cancellable;
    }

    Progress get_progress(string client) {
        return clients[client].progress;
    }

    void cancel_client(string client) {
        if (!clients.contains(client)) {
            warning("Unregistered client: %s!", client);
            return;
        }
        clients[client].cancellable.cancel();
        clients[client].cancellable = null;
    }

    void error_if_live() throws UpdateError {
        if (is_liveos)
            throw new UpdateError.LIVE_OS("Unavailable on a live system");
    }
}

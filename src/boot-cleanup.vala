using Linux;
using OSTree;

void main() {
    // Enter a mount namespace
    if (unshare(CloneFlags.NEWNS) != 0)
        error("Couldn't set up mount namespace: %m");

    // Initialize the sysroot
    var env_sysroot = Environment.get_variable("OSTREE_SYSROOT");
    File sysroot_file = null;
    if (env_sysroot != null) sysroot_file = File.new_for_path(env_sysroot);
    var sysroot = new Sysroot(sysroot_file);
    sysroot.set_mount_namespace_in_use();
    try {
        sysroot.@lock();
        sysroot.load();
    } catch (Error e) {
        error("Failed to obtain sysroot: %s", e.message);
    }
    message("Found sysroot: %s", sysroot.path.get_path());

    // Find the deployments that are pending deletion & undeploy them
    Deployment[] victims = {};
    var deployments = sysroot.get_deployments();
    unowned var? booted = sysroot.get_booted_deployment();
    deployments.foreach(it => {
        unowned var origin = it.get_origin();
        bool remove;
        try {
            remove = origin.get_boolean("libostree-transient", "x-pending-delete");
        } catch {
            remove = false;
        }
        if (remove && !it.is_pinned() && it != booted)
            victims += it;
    });
    if (victims.length > 0) {
        message("Starting undeploy");
        foreach (var it in victims) {
            message("Removing %s.%d", it.get_csum(), it.get_bootserial());
            deployments.remove(it);
        }
        var write_opts = SysrootWriteDeploymentsOpts() {
            do_postclean = false
        };
        try {
            sysroot.write_deployments_with_options(deployments, write_opts);
        } catch (Error e) {
            error("Failed to write deployments: %s", e.message);
        }
    } else {
        message("Skipping undeploy. Nothing to do");
    }

    // Do the cleanup/prune
    message("Starting cleanup");
    try {
        sysroot.cleanup();
    } catch (Error e) {
        error("Failed to cleanup: %s", e.message);
    }
}

using Updated;

namespace OTUtils {
    // Returns an uninitialized OSTree sysroot
    private static OSTree.Sysroot get_sysroot_uninitialized(string path) {
        File sysroot_file = null;
        if (path != "") sysroot_file = File.new_for_path(path);
        var sysroot = new OSTree.Sysroot(sysroot_file);
        sysroot.set_mount_namespace_in_use();
        return sysroot;
    }

    // Utility function to get an OSTree sysroot from an opt_sysroot arg,
    // without locking the repository
    public OSTree.Sysroot get_sysroot_no_lock(string path, Cancellable? c)
            throws UpdateError {
        var sysroot = get_sysroot_uninitialized(path);

        try {
            sysroot.load(c);
        } catch {
            Utils.bail_if_cancelled(c);
            throw new UpdateError.FAILED("Failed to load sysroot");
        }

        return sysroot;
    }

    // Utility function to get an OSTree sysroot from an opt_sysroot arg
    public OSTree.Sysroot get_sysroot(string path, Cancellable? c)
            throws UpdateError {
        var sysroot = get_sysroot_uninitialized(path);

        var have_lock = false;
        try {
            sysroot.try_lock(out have_lock);
        } catch (Error e) {
            warning("Failed to try_lock! %s", e.message);
        }
        if (!have_lock)
            throw new UpdateError.NO_LOCK("System update mechanism is busy");

        try {
            sysroot.load(c);
        } catch {
            throw new UpdateError.FAILED("Failed to load sysroot");
        }

        return sysroot;
    }

    // Get origin from a deployment w/ fallback
    public KeyFile get_origin(OSTree.Deployment? merge_deploy) {
        KeyFile? origin = null;

        // Try to get origin from an existing deployment
        if (merge_deploy != null)
            origin = merge_deploy.get_origin();

        // Fallback to standard defaults
        if (origin == null) {
            origin = new KeyFile();
            origin.set_string("origin", "BaseVariant", "desktop");
            origin.set_string("origin", "KernelVariant", "mainline");
            origin.set_string("origin", "Collection", "sh.carbon.Stable");
        }

        // Strip out data that shouldn't leak b/t deployments
        OSTree.Deployment.origin_remove_transient_state(origin);

        return origin;
    }

    public bool check_pending_deletion(OSTree.Deployment? deploy) {
        if (deploy == null) return false;
        try {
            var origin = deploy.get_origin();
            return origin.get_boolean("libostree-transient", "x-pending-delete");
        } catch {
            return false;
        }
    }

    public HashTable<string, Variant> get_commit_metadata(Variant commit) {
        return commit.get_child_value(0) as HashTable<string, Variant>;
    }

    public void ensure_writable(OSTree.Repo repo) throws UpdateError {
        bool writable = false;
        try {
            writable = repo.is_writable();
        } catch (Error e) {
            critical("Failed to check if repo is writable: %s", e.message);
        }

        if (!writable)
            throw new UpdateError.ACCESS_DENIED("Repository is not writable");
    }

    // Parses the origin & returns the arch
    public string parse_origin(KeyFile origin, out OSTree.CollectionRef base_cref,
            out OSTree.CollectionRef kernel_cref) throws UpdateError {
        string arch, collection_id, base_variant, kernel_variant;
        try {
            collection_id = origin.get_string("origin", "Collection");
            base_variant = origin.get_string("origin", "BaseVariant");
            kernel_variant = origin.get_string("origin", "KernelVariant");
        } catch (Error e) {
            throw new UpdateError.FAILED("Malformed origin: " + e.message);
        }
        arch = Posix.utsname().machine; // equiv to `uname -m`
        var base_refspec = "os/base/%s/%s".printf(arch, base_variant);
        var kernel_refspec = "os/kernel/%s/%s".printf(arch, kernel_variant);
        base_cref = new OSTree.CollectionRef(collection_id, base_refspec);
        kernel_cref = new OSTree.CollectionRef(collection_id, kernel_refspec);
        return arch;
    }
}

namespace Utils {
    public async void auth(string sender, string action, Cancellable c)
            throws UpdateError {
        bool authorized;
        try {
            var authority = yield Polkit.Authority.get_async(c);
            var subject = new Polkit.SystemBusName(sender);
            var result = yield authority.check_authorization(subject, action,
                null, Polkit.CheckAuthorizationFlags.ALLOW_USER_INTERACTION, c);
            authorized = result.get_is_authorized();
        } catch (Error e) {
            critical("Failed to request authorization: %s", e.message);
            authorized = false;
        }

        if (!authorized)
            throw new UpdateError.ACCESS_DENIED("Permission denied");
    }

    public inline void bail_if_cancelled(Cancellable c) throws UpdateError {
        if (c.is_cancelled()) throw new UpdateError.CANCELLED("Cancelled");
    }
}

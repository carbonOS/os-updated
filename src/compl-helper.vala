using Updated;

// https://gitlab.com/carbonOS/os-updated/-/issues/17#note_847426688
void print_deployment_names(Deployment[] deployments, bool pinned_only) {
    var idx = -1;
    var print_current = false;
    foreach (var deployment in deployments) {
        idx++;

        if (deployment.is_pending_delete)
            continue;
        if (pinned_only && !deployment.is_pinned)
            continue;

        print("%d\n", idx);
        print("%s.%d\n", deployment.csum[:8], deployment.deploy_serial);
        if (deployment.is_rollback)
            print("rollback\n");
        if (deployment.is_booted) {
            print("booted\n");
            print_current = true;
        }
        if (deployment.is_pending) {
            print("pending\n");
            print_current = true;
        }
    }
    if (print_current)
        print("current\n");
}

void main(string[] args) {
    if (args.length != 2) error("Invalid arguments");

    // Connect to the service
    var service = Service.create();

    // Get the sysroot
    var sysroot = Environment.get_variable("OSTREE_SYSROOT");
	if (sysroot != null) sysroot = Filename.canonicalize(sysroot);
    sysroot = sysroot ?? ""; // daemon expects "" instead of null

    // Get the deployments
    var booted = false;
    var deployments = service.get_deployments(sysroot, out booted);

    switch (args[1]) {
        case "dnames":
            print_deployment_names(deployments, false);
            break;
        case "pinned-dnames":
            print_deployment_names(deployments, true);
            break;
        default:
            error("Invalid arg: %s", args[1]);
    }
}

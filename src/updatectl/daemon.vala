using Cli;
using Updated;

namespace Daemon {

    // TODO: Remove
    Service get_main_service() throws Error {
        return Service.create();
    }

    class TaskOutput {
        public string? create_dirs_msg = "Creating directories";
        public string? resolving_msg = "Resolving remotes";
        public string? pulling_msg = "Downloading content";
        public string? merging_msg = "Writing OSTree commit";
        public string? deploying_msg = "Staging deployment";
        public string? pruning_msg = "Cleaning up";

        private Progress daemon_progress;
        private Error? daemon_error = null;
        private MainContext main_ctx;
	    private string? status = null;
	    private uint spinner_timeout = 0;
	    private int spinner_frame = 0;
	    private double progress = -1;
	    private string? progress_status = null;
	    private bool has_fine_progress = false;
	    private int term_width;
	    private string old_line = "";

        public TaskOutput(Service daemon) throws Error {
            main_ctx = MainContext.@default();
            term_width = get_term_width();

            daemon_progress = daemon.get_progress();
            daemon_progress.creating_dirs.connect(() => status = create_dirs_msg);
            daemon_progress.resolving.connect(() => status = resolving_msg);
            daemon_progress.pulling.connect(() => {
                status = pulling_msg;
                has_fine_progress = false; // New pull might not have it
            });
            daemon_progress.downloaded.connect((done, full) => {
                has_fine_progress = true;
                progress = (double) done / full;
                progress_status = "%.0f%% (%s/%s)".printf(progress * 100,
                    format_size(done), format_size(full));
            });
            daemon_progress.downloaded_coarse.connect((done, full) => {
                if (has_fine_progress) return; // Don't need coarse progress
                progress = (double) done / full;
                progress_status = "%.0f%% (%u/%u obj)".printf(progress * 100,
                    done, full);
            });
            daemon_progress.merging.connect(() => status = merging_msg);
            daemon_progress.deploying.connect(() => status = deploying_msg);
            daemon_progress.pruning.connect(() => status = pruning_msg);
            daemon_progress.finished.connect(() => finish(true));

            spinner_timeout = Timeout.add(125, () => {
                spinner_frame++;
                spinner_frame %= 8;
                return Source.CONTINUE;
            });
        }

        ~TaskOutput() {
            Source.remove(spinner_timeout);
        }

        public void failed(Error e) {
            daemon_error = e;
            finish(false);
        }

        private void finish(bool successful) {
            if (status == null) return;
            var indicator = successful ? GREEN + Glyph.PASS.to_string() :
                RED + Glyph.FAIL.to_string();
            print(CLR_LINE + BOLD + indicator + RESET + " " + status + "\n");

            // Reset
            status = null;
            spinner_frame = 0;
            progress = -1;
            progress_status = null;
            old_line = "";
        }

	    public void iteration() throws Error {
	        if (daemon_error != null)
	            throw daemon_error;

	        main_ctx.iteration(false);
            if (status == null) return;

            var spinner = Glyph.SPINNER.to_string(spinner_frame);
            var tail = "...";
            if (progress != -1) {
                var pbar = progress_bar(progress, term_width - 100);
                tail = ": %s%s".printf(pbar, progress_status);
            }

            // Print the line if it changed
            var new_line = "%s %s%s".printf(spinner, status, tail);
            if (new_line != old_line) {
                old_line = new_line;
                print(CLR_LINE + new_line);
            }
        }
    }
}
